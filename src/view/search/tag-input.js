
/**
 * Represents the form for tag inputs
 */
class TagInput extends Autowire {

    onAttach(){
        this.input = this.q('input');
        this.menu = this.q('.tag-menu');
        this.added_tags = this.q('.added-tags');

        this.input.addEventListener('keyup', this.search_tags.bind(this));

        this.last_poll = 0;
        this.max_poll_frequency_ms = 400;
        this.is_timeout_waiting = false;
        this.last_input = this.input.value || undefined;
    }

    onReady(){
        const existing_tag_btns = document.querySelectorAll('.DecaturVoteSearch .tag-input-form .added-tags button');
        for (const btn of existing_tag_btns){
            btn.addEventListener('click', this.tag_removed.bind(this), {once:true});
        }
    }

    /** get uri for querying for a list of tags */
    get_uri(input_value){
        return '/search/get-tags/?tag_name='+input_value;
    }
    /** get uri for creating a new tag */
    get_create_uri(new_tag_name){
        return '/search/create-tag/?tag_name='+new_tag_name;
    }

    /**
     * Get an array of tags for the given input string, matching part or all of a tag name
     * @param tag_name a full or partial tag name
     * @return array<int index, SearchTag tag> array of `SearchTag`s TODO
     */
    async get_tags_for_input(tag_name){

        this.last_input = this.input.value;
        this.is_timeout_waiting = false;
        this.last_poll = Date.now();

        const uri = this.get_uri(this.input.value);
        const response = await (await fetch(uri)).json();
        const tags = response.map((row)=>Object.assign(new SearchTag(), row));

        return tags;
    }

    /**
     * Query for tags and display them for user-selection, based on user input.
     */
    async search_tags(event){
        // LIMIT polling frequency
        const cur_poll = Date.now();
        const diff = cur_poll - this.last_poll ;

        if (this.input.value === this.last_input){
            return;
        }
        if (this.is_timeout_waiting == false && diff < this.max_poll_frequency_ms ){
            this.is_timeout_waiting = true;
            setTimeout(this.search_tags.bind(this), this.max_poll_frequency_ms - diff);
            // console.log("Set timeout. Diff "+diff);
            return;
        }

        // GET tags from server
        const tags = await this.get_tags_for_input(this.input.value);


        // DISPLAY tags for user-selection
        this.menu.innerHTML = '';
        let is_present = false;
        for (const tag of tags){
            const node = document.createElement('button');
            node.innerText = tag.name;
            node.setAttribute('data-id', tag.id);
            node.addEventListener('click',this.tag_selected.bind(this), {once:true});
            this.menu.appendChild(node);
            if (tag.name.toLowerCase()==this.input.value.toLowerCase())is_present = true;
        }
        if (!is_present){
            const node = document.createElement('button');
            node.innerText = '+'+this.input.value;
            node.classList.add('new-tag');
            node.setAttribute('data-id', 'NEW');
            node.setAttribute('data-name', this.input.value);
            node.addEventListener('click',this.new_tag_selected.bind(this), {once:true});
            this.menu.insertBefore(node, this.menu.firstChild);
        }
    }

    /**
     * Create a tag server-side and return the newly created tag.
     * @param tag_name string tag name
     * @return SearchTag a tag
     */
    async create_tag(tag_name){
        const uri = this.get_create_uri(tag_name);
        const response = await (await fetch(uri)).json();

        const tag = Object.assign(new SearchTag(), response);

        return tag;
    }

    /**
     * Create a new tag server-side, get the new tag's id, update the button, then call tag_selected() to add it to the form.
     */
    async new_tag_selected(event){
        event.stopPropagation();
        event.preventDefault();
        const btn = event.target;
        const name = btn.getAttribute('data-name');
        const tag = await this.create_tag(name);
        btn.setAttribute('data-id', tag.id);
        btn.innerText = tag.name;

        this.tag_selected(event);
    }

    /**
     * Handle when a tag is clicked by the user, and add it to the form.
     */
    tag_selected(event){
        event.stopPropagation();
        event.preventDefault();
        const n = event.target;
        n.parentNode.removeChild(n);
        n.addEventListener('click', this.tag_removed.bind(this), {once:true});
        this.menu.innerHTML = '';
        this.input.value = '';

        const name = n.innerText;
        const id = n.getAttribute('data-id');

        const input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', 'tag[]');
        input.setAttribute('value', id);

        this.added_tags.appendChild(n);
        this.added_tags.appendChild(input);

    }

    /**
     * Handle when an already-selected tag is clicked by the user
     */
    tag_removed(event){
        event.stopPropagation();
        event.preventDefault();
        const id = event.target.getAttribute('data-id');
        const selector= 'input[name="tag[]"][value="'+id+'"]';
        console.log(selector);
        const id_node = this.q(selector);

        event.target.parentNode.removeChild(event.target);
        id_node.parentNode.removeChild(id_node);
    }
}
// its not technically a form, bc I want to prevent accidental submissions.
TagInput.autowire('.DecaturVoteSearch .tag-input-form');
