/**
 * Contains value-object classes for json objects returned from json_results.php
 */


/**
 * Simple value object defining properties of a Search Result Row
 */
class SearchResultRow {

    /** The internal int id of the SearchResult */
    id;
    /** user-friendly title of the search item */
    title;
    /** user-friendly summary of the search item */
    summary;
    /** string like 'article' or 'source' */
    type;
    /** datetime STRING like '2023-08-29 7:40 pm' */
    updated_at;
    /** URL to redirect user to */
    uri;

}

/** 
 * Simple value object defining properties of a Selectable Tag, returned from a query.
 */
class TagRow {

    /** internal int id of the tag */
    id;
    /** string name of tag */
    name;
    /** int, number of results form current set that match this tag */
    count;

}

/** Simple value object defining properties of a selectable Type, returned from a query */
class TypeRow {
    /** string type */
    type;
    /** int count of results with this type */
    c;
}
