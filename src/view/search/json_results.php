<?php
/**
 * Get search results and tag list as json.
 *
 * @param $search_items array<int index, DecaturVote\SearchDb\Search $search_item_orm> of all items returned from search.
 * @param $tags array<int index, DecaturVote\SearchDb\Tag $tag_orm> of all tags that can be used to refine this search.
 * @param $available_types array<int index, array type> of all types available for filtering. type contains string 'type' and count 'c'.
 * @param $paginator \DecaturVote\Search\Paginator
 *
 * @output json, may be empty
 *
 * @note does not exit. Calling script should `exit`
 */

// if (!isset($query)){
    // echo '{"message":"ERROR: pass \'query\'=>\'Query String\'"}';
    // return;
// }

$results = array_map(
    function(\DecaturVote\SearchDb\Search $item) {

        $summary = $item->summary;
        if (strlen($summary)>300)$summary = substr($summary, 0, 300).'...';

        return [
            'id'=>$item->id,
            'title'=>$item->title,
            'summary'=>$summary,
            'uri' => $item->uri,
            'updated_at' => $item->updatedAt,
            'type'=>$item->type,
        ];
    },
    $search_items
);

$tags = array_map(
    function(\DecaturVote\SearchDb\Tag $tag) {

        return [
            'name'=>$tag->tag_name,
            'id'=>$tag->id,
            'count'=>$tag->item_count,
        ];
    },
    $tags
);

$page_links = [];
foreach ($paginator->get_links(7) as $page_num=>$url){
    if ($page_num==$paginator->current_page+1){
        $page_links[] = "<a class=\"current_page\" href=\"$url\">$page_num</a>";
    } else {
        $page_links[] = "<a href=\"$url\">$page_num</a>";
    }
}

$data = [
    'results'=>$results,
    'tags'=>$tags,
    'types'=>$available_types,
    'page_links'=> $page_links,
];

echo json_encode($data, JSON_PRETTY_PRINT);

return;
