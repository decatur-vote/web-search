<?php
/**
 * Display a form that looks up tags
 *
 * @param $item_uuid OPTIONAL the uuid of the item for which tags will be added. This is used to look up existing tags.
 * @param $tag_list OPTIONAL string of tags in format `tag_name:tag_id, tag2:tagid2`. Tag id should be int from autoincrement column. This is ONLY used if `$item_uuid` is null
 * @param $existing_tags array<int index, DecaturVote\SearchDb\Tag tag> OPTIONAL array of tags to display
 */
$package->integration->add_tag_will_display($lia);
$lia->addResourceFile(__DIR__.'/SearchTag.js');

if (!isset($existing_tags)){
    $existing_tags = [];
    if (isset($item_uuid)&&$item_uuid != null){
        $db = new \DecaturVote\SearchDb($package->integration->getPdo());
        $existing_tags = $db->query('tag', 'by_item_uuid', ['item_uuid'=>$item_uuid]);
    } else if (isset($tag_list) && is_string($tag_list)){
        $db = new \DecaturVote\SearchDb($package->integration->getPdo());
        $existing_tags = [];
        $tags = explode(',',$tag_list);
        foreach ($tags as $tag){
            $parts = explode(':', $tag);
            $existing_tag = new DecaturVote\SearchDb\Tag($db);
            if (!isset($parts[1])||!is_numeric($parts[1])||!is_string($parts[0]))continue;
            $existing_tag->id = $parts[1];
            $existing_tag->tag_name = $parts[0];
            $existing_tags[] = $existing_tag;
        }
    }
}

?>
<div class="DecaturVoteSearch">
    <div class="tag-input-form">
        <input data-style="min-width:80%;" type="text" name="tag_name" placeholder="tag-name" autocomplete="off" />

        <div class="tag-menu">
        </div>

        <div class="added-tags">
            <?php foreach ($existing_tags as $tag): ?>
                <button data-id="<?=$tag->id?>"><?=$tag->tag_name?></button>
                <input type="hidden" name="tag[]" value="<?=$tag->id?>">
            <?php endforeach; ?>
        </div>

    </div>

</div>
