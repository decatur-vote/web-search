/**
 * Contains classes for search interface: Search Result, SearchTag, and SearchTagAsButton
 */


/**
 * A simple search result. 
 */
class SearchResult extends Autowire {

    /**
     * Modify html content with values from a SearchResultRow.
     * 
     * @param row a SearchResultRow object
     */
    bind_row(row){
        this.q('.title a').innerHTML = row.title;
        this.q('.title a').href = row.uri;
        this.q('.summary').innerHTML = row.summary;
        this.q('.date').innerHTML = row.updated_at;
        this.q('.type').innerHTML = row.type;
    }
}

/**
 * Manages a label & checkbox for a tag + interface for replacing itself with a button.
 */
class SearchTag extends Autowire {

    onCreate(searchPage){
        /** SearchPage object */
        this.searchPage = searchPage;
        /** A TagRow or TypeRow object */
        this.row = null;
        /** The SearchTagAsButton object used for user-interactions, which replaces the label/checkbox */
        this.button = null;
    }

    onAttach(){
        if (this.n.parentNode!=null){
            this.convertToButton();
        }
    }

    get input(){
        return this.q('input');
    }
    get label(){
        return this.n;
    }
    get is_checked(){
        return this.input.checked;
    }

    toggle(){
        // const event = new Event('change');
        // this.input.dispatchEvent(event);
        // this.input.checked = !this.is_checked;
        this.input.click();
    }

    bind_row(row){
        this.q('.name').innerHTML = row.name;
        this.q('input').value = row.id;
        this.q('input').name = row.input_name;
        this.q('.count').innerHTML = '('+row.count+')';

        // this.convertToButton();
    }

    convertToButton(){
        if (this.button!=null)return;
        this.button = new SearchTagAsButton('.ButtonTagTemplate', this.searchPage, this);
        this.n.parentNode.insertBefore(this.button.n, this.n);
        this.label.style.display = 'none';
        this.button.update_state();
    }
}

class SearchTagAsButton extends Autowire {


    onCreate(searchPage, searchTag){
        /** SearchPage instance */
        this.searchPage = searchPage;
        /** SearchTag instance that this button replaces in the ui */
        this.searchTag = searchTag;
    
        this.q('.name').innerHTML = searchTag.q('.name').innerHTML;
        this.q('.count').innerHTML = searchTag.q('.count').innerHTML;
    }

    onclick(event){
        event.preventDefault();
        event.stopPropagation();

        this.searchTag.toggle();
        this.update_state();
    }

    /**
     * add css class 'selected' if the attached checkbox is checked. remove it otherwise
     */
    update_state(){
        const state = this.searchTag.is_checked;
        if (state)this.n.classList.add('selected');
        else this.n.classList.remove('selected');
    }

}

