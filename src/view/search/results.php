<?php
/**
 * Display the search results (with no nav items)
 * @param $items array<int index, \DecaturVote\SearchDb\Search $orm_objects>
 */

?>
<div class="ResultsList">

    <?php
            if (count($items) == 0){
                echo "<h2>No results found</h2>";
            }
            foreach ($items as $item): 
                $summary = $item->summary;
                if (strlen($summary)>300)$summary = substr($summary, 0, 300).'...';
                ?>
                <div class="SearchResult">
                    <h2 class="title"><a href="<?=$item->uri?>"><?=$item->title?></a></h2>
                    <p class="summary"><?=$summary?></p>
                    <div class="search_footer">
                        <small class="type"><?=$item->type?></small>
                        <small class="date"><?=$item->updatedAt?></small>
                    </div>
                </div>
            <?php endforeach;
        // }
    ?>
</div>
