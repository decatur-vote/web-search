<?php
/**
 *
 *
 * @see RSS spec https://validator.w3.org/feed/docs/rss2.html 
 * @see sample of version 2.0 rss: https://www.rssboard.org/files/sample-rss-2.xml
 *
 * @param $items array<int index, \DecaturVote\SearchDb\Search $orm_item> array of search items
 * @param $channel array<string key, string value> array of organization information for the rss `<channel>` node, per the rss 2.0 spec.
 */
?>
<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
   <channel>
   <atom:link href="<?=$channel['link']??''?>" rel="self" type="application/rss+xml" />
    <?php 
        foreach ($channel as $key=>$value){ 
            echo "<$key>$value</$key>\n";
        }
    ?>
    
    <?php foreach ($items as $item): 
        $url = $item->uri;
        $scheme = parse_url($url,PHP_URL_SCHEME);
        if ($scheme===false || $scheme===null){
            $url = $package->integration->getHostWebsite().$url;
        }
            // <pubDate><?=$item->dateTime->format("D, d M Y H:i:s T")? ></pubDate>
    ?>

        <item>
            <title><?=$item->title?></title>
            <link><?=$url?></link>
            <description><?=$item->summary?></description>
            <pubDate><?=$item->dateTime->format(\DateTime::RSS)?></pubDate>
            <guid isPermaLink="false"><?=$item->uuid?></guid>

        </item>
    <?php endforeach; ?>
   </channel>
</rss>
