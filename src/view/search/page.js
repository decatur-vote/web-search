
/**
 * Global parent class for managing a search page.
 * This class conducts queries, displays results, and displays updated tag & type lists for user-interaction.
 */
class SearchPage extends Autowire {

    onAttach(){
        this.input.addEventListener('keyup',this.search.bind(this));
        this.form.addEventListener('change', this.checkbox_changed.bind(this));
        this.last_poll = 0;
        this.max_poll_frequency_ms = 400;
        this.is_timeout_waiting = false;
        /** The text query string. Search only runs if the last_input is different from current input. Set to undefined, then call search(), in order to force a search. */
        this.last_input = this.input.value || undefined;
        window.addEventListener("popstate", this.state_popped.bind(this));

        SearchTag.autowire('label.TagCheckboxLabel', true, this);

    }

    state_popped(event){
        // window.location.href = window.location.href;
        location.reload();
    }
    /** parent node where results will be added to */
    get results_node(){
        return this.q('.ResultsList');
    }

    /** text input for query string */
    get input(){
        return this.q('input[name="q"]');
    }
    /** The form containing text input & tags */
    get form(){
        return this.q('form');
    }

    /** 
     * Triggered when a checkbox is toggled (i.e. tag enabled/disabled)
     */
    checkbox_changed(event){
        if (event.target.getAttribute('type') != 'checkbox')return;
        const checkbox = event.target;
        /** Set undefined to force search() to work */
        this.last_input = undefined;
        this.search(null);
        checkbox.parentNode.classList.toggle('selected');

        
    }

    /** 
     * Construct a search uri from the query string input & selected tags
     * @return a url for performing search.
     */
    buildSearchUri(){

        let uri = this.form.action+'?'+this.input.name+'='+this.input.value;

        const search_types = this.qa('input[name^=search_for]');
        for (const type of search_types){
            if (!type.checked)continue;
            uri += '&search_for[]='+type.value;
        }

        const tags = this.qa('input[name^=tags]');
        for (const tag of tags){
            if (!tag.checked)continue;
            uri += '&tags[]='+tag.value;
        }

        return uri;
    }

    /**
     * Called by setTimeout(). Used to ensure we don't run multiple queries at the same time
     */
    timeout_search(event){
        this.is_awaiting_timeout = false;
        this.search(event);
    }

    /**
     * Check if the last search was shorter than `this.max_poll_frequency_ms` ago.
     * @return true/false
     */
    is_search_too_frequent(){
        const cur_poll = Date.now();
        const diff = cur_poll - this.last_poll ;
        if (diff < this.max_poll_frequency_ms)return true;

        return false;
    }

    /**
     * Display query results.
     *
     * @param results array of SearchResultRow instances
     */
    show_results(results){
        this.results_node.innerHTML = '';
        if (results.length==0){
            this.results_node.innerHTML = '<h2>No Results Found</h2>';   
        }
        for (const row of results){
            // console.log(row);
            const Result = new SearchResult('.DecaturVoteSearch template.SearchResultTemplate');
            // console.log(Result.n);
            this.results_node.appendChild(Result.node);
            Result.bind_row(row);
        }
    }

    /**
     * Display tags for query refinement
     *
     * @param tags array of TagRow instances
     */
    show_tags(tags){
        const selected_tags = this.get_selected_tag_ids();

        const tag_list = this.q('.TagList');
        tag_list.innerHTML = 'Filter: ';
        for (const tag_row of tags){
            const Tag = new SearchTag('.DecaturVoteSearch template.TagTemplate', this);
            tag_list.appendChild(Tag.node);
            // console.log({"parent": Tag.node.parentNode});
            tag_row['input_name'] = 'tags[]';
            Tag.bind_row(tag_row);
            // if this tag was already selected, mark the new (replacement) node as selected.
            if (selected_tags.includes(tag_row.id)){
                Tag.input.checked = true;
                Tag.label.classList.add('selected');
            }
            Tag.convertToButton();
        }
    }

    /**
     * Display type-buttons for query refinement
     * @param types array of TypeRow instances
     */
    show_types(types){
        const selected_types = this.get_selected_types();
        const type_list = this.q('.TypeList');
        type_list.innerHTML = 'Search For: ';
        for (const type_row of types){
            const Tag = new SearchTag('.DecaturVoteSearch template.TagTemplate', this);
            type_list.appendChild(Tag.node);
            console.log({"parent": Tag.node.parentNode});
            // type_row['input_name'] = 'tags[]';
            const row = {
                name: type_row['type'],
                id: type_row['type'],
                count: type_row['c'],
                input_name: 'search_for[]'
            };
            Tag.bind_row(row);
            if (selected_types.includes(row.id)){
                // Tag.input.setAttribute('checked','checked');
                Tag.input.checked = true;
                Tag.label.classList.add('selected');
                // console.log('check it!');
            }
            Tag.convertToButton();
        }
    }

    async search(event){

        // DO NOT QUERY if last query string is same as current
        if (this.input.value === this.last_input){
            return;
        }


        // BUILD URI
        const page_url = this.buildSearchUri();
        const json_uri = page_url + '&format=json';

        if (this.is_awaiting_timeout)return;
        if (this.is_search_too_frequent()){
            this.is_awaiting_timeout = true;
            // we add 1 to help prevent timing errors
            const cur_poll = Date.now();
            const diff = cur_poll - this.last_poll ;
            setTimeout(this.timeout_search.bind(this), this.max_poll_frequency_ms - diff + 1);
            return;
        }

        this.last_input = this.input.value;
        this.last_poll = Date.now();

        const response = await (await fetch(json_uri)).json();

        const results = response.results.map((row)=>Object.assign(new SearchResultRow(), row));
        const tags = response.tags.map((row)=>Object.assign(new TagRow(), row));
        const types = response.types.map((row)=>Object.assign(new TypeRow(), row));

        this.show_results(results);
        this.show_tags(tags);
        this.show_types(types);


        // UPDATE pagination links
        let link_html = '';
        for (const link of response.page_links){
            // 'link' is an html <a> node
            link_html += link+"";
        }
        for (const nav of this.qa('nav.pages')){
            nav.innerHTML = link_html;
        }

        // ADD this search to browser nav history
        window.history.pushState({html:document.body.outerHTML}, 'search '+ this.input.value, page_url);

        // UPDATE feed uris for html feed, json feed, rss feed.
        const feed_uri = page_url.replace(this.form.action, '/feed/', page_url);
        this.q('.feeds a.html.feed').href = feed_uri;
        this.q('.feeds a.json.feed').href = feed_uri+'&format=json';
        this.q('.feeds a.rss.feed').href = feed_uri+'&format=rss';
    }

    /** 
     * Get an array of selected tag ids.
     *
     * @return array<int index, int tag_id>
     */
    get_selected_tag_ids(){
        const selected_ids = [];
        for (const element of this.qa('.TagList input')){
            if (!element.checked)continue;
            selected_ids.push(Number(element.value));
        }
        return selected_ids;
    }

    /** 
     * Get an array of selected type strings.
     *
     * @return array<int index, string type>
     */
    get_selected_types(){
        const selected_types = [];
        for (const element of this.qa('.TypeList input')){
            if (!element.checked)continue;
            selected_types.push(element.value);
        }
        return selected_types;
    }

}
