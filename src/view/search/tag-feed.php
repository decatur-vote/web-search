<?php
/**
 * Display a feed of a single tag, with only one nav control, pointing to the source feed
 *
 * @param ?$tag string name of a tag to provide a feed for. May be null to display feed of everything.
 * @param ?$type optional string type of search item to display
 */

$db = new \DecaturVote\SearchDb($package->integration->getPdo());


$package->integration->tag_feed_will_display($lia);

if (isset($tag)){
    $tag_id = $db->query_rows('tag.by_name', ['name'=>$tag])[0]['id']??null;
    $tags = [$tag_id];
    $tags_query = 'tags[]='.$tag_id;
} else {
    $tags = [];
    $tags_query = '';
}

$lia->addResourceFile(__DIR__.'/page.css');

$types = [];
if (isset($type)){
    $types = [$type];
    $type_query = 'search_for[]='.$type;
    if ($tags_query!='')$tags_query .= '&';
}

$items = $db->feed('',$types,$tags,'LIMIT 0,10');


?>
<div class="DecaturVoteSearch">
    <?=$lia->view('search/results', ['items'=>$items]); ?>

    <nav class="feeds">
        <a class="html feed" href="/feed/?<?=$tags_query.$type_query?>">[feed]</a>
    </nav>
</div>
