
/** 
 * Simple value object defining a tag, used for adding tags to articles or other items.
 */
class SearchTag {

    /** internal int id of the tag */
    id;
    /** string name of tag */
    name;

}
