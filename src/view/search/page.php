<?php
/**
 * @param $search_term string the query string passed by the user
 * @param $search_items array<int index, DecaturVote\SearchDb\Search $search_item_orm> of all items returned from search.
 * @param ?$selected_types array<int index, string item_type> array of item types that are selected
 * @param ?$selected_tags array<int index, int tag_id> array of tags that are selected
 * @param $available_tags array<int index, DecaturVote\SearchDb\Tag $tag_item_orm> of all tags available for filtering
 * @param $available_types array<int index, array type> of all types available for filtering. type contains string 'type' and count 'c'.
 * @param $paginator \DecaturVote\Search\Paginator
 * @param ?$show_controls default true, optional. Set `false` to not show search input, tag filtering, etc.
 *
 * @output html
 */

$selected_types = empty($selected_types) ? [] : array_combine($selected_types, $selected_types);
$selected_tags = empty($selected_tags) ? [] : array_combine($selected_tags, $selected_tags);

$package->integration->page_will_display($lia);

$searchDb = new \DecaturVote\SearchDb($package->integration->getPdo());

$tags = $available_tags ?? [];
$types = $available_types ?? [];


$show_controls = $show_controls ?? true;

?>

<div class="DecaturVoteSearch">
    <?php if ($show_controls): ?>
    <h1>Search</h1>
    <form method="GET" action="/search/" autocomplete="off">
    <input data-style="min-width:80%;" type="text" name="q" placeholder="search..." value="<?=$search_term?>"/>
<br>

        <div style="padding:8px;" class="TypeList">
            Search For:
            <?php foreach ($types as $type): 
                $name = $type['type'];
                $count = $type['c'];
                // $id = $type['id'];
                $checked = isset($selected_types[$name]) ? ' checked' : '';
            ?>
                
                <label class="TagCheckboxLabel">
                    <span class="name"><?=$name?></span><i class="count">(<?=$count?>)</i>
                    <input type="checkbox" name="search_for[]" value="<?=$name?>" <?=$checked?>>
                </label>
            <?php endforeach;?>
        </div>


        <div style="padding:8px;" class="TagList">
            Filter:
            <?php foreach ($tags as $tag): 
                $name = $tag->tag_name;
                $id = $tag->id;
                $count = $tag->item_count;
                // $id = $type['id'];
                $checked = isset($selected_tags[$id]) ? ' checked' : '';
            ?>
                <label class="TagCheckboxLabel">
                    <span class="name"><?=$name?></span><i class="count">(<?=$count?>)</i>
                    <input type="checkbox" name="tags[]" value="<?=$id?>" <?=$checked?>>
                </label>
            <?php endforeach;?>
        </div>

        <noscript><input type="submit" value="Search"></noscript>
    </form>
    <br>
    <?php endif;?>

<br>

    <nav class="pages">
    <?php
        foreach ($paginator->get_links(7) as $page_num=>$url){
            if ($page_num==$paginator->current_page+1){
                echo "<a class=\"current_page\" href=\"$url\">$page_num</a>";
            } else {
                echo "<a href=\"$url\">$page_num</a>";
            }
        }

    ?>
    </nav>
    <?=$lia->view('search/results', ['items'=>$search_items]);?>
    <nav class="pages">
    <?php
        foreach ($paginator->get_links(7) as $page_num=>$url){
            if ($page_num==$paginator->current_page+1){
                echo "<a class=\"current_page\" href=\"$url\">$page_num</a>";
            } else {
                echo "<a href=\"$url\">$page_num</a>";
            }
        }
    ?>
    </nav>
    <nav class="feeds">
        <a class="reset_search" href="/search/">[reset search]</a>
        <a class="html feed" href="/feed/?<?=http_build_query($_GET)?>">[feed]</a>
        <a class="rss feed" href="/feed/?format=rss&<?=http_build_query($_GET)?>">[rss]</a>
        <a class="json feed" href="/feed/?format=json&<?=http_build_query($_GET)?>">[json]</a>
    </nav>

    <template class="SearchResultTemplate">
        <div class="SearchResult">
            <h2 class="title"><a href="#">Error...</a></h2>
            <p class="summary"></p>
            <div class="search_footer">
                <small class="type"></small>
                <small class="date"></small>
            </div>
        </div>
    </template>
    <template class="TagTemplate">
        <label class="TagCheckboxLabel">
            <span class="name"></span>
            <i class="count"></i>
            <input type="checkbox" name="" value="">
        </label>
    </template>
    <template class="ButtonTagTemplate">
        <button class="SearchTagButton">
            <span class="name"></span> 
            <i class="count"></i>
        </button>
    </template>
</div>

<script type="text/javascript">
    SearchPage.autowire('.DecaturVoteSearch');
</script>
