<?php

namespace DecaturVote;

class SearchDb extends \Tlf\BigDb {

    protected string $orm_namespace = 'DecaturVote\\SearchDb';

    public function get_orm_class(string $table): string {
        $map = [
            'search'=>\DecaturVote\SearchDb\Search::class,
            'tag'=>\DecaturVote\SearchDb\Tag::class,
            'tagged_by'=>\DecaturVote\SearchDb\TaggedBy::class,
        ];

        return $map[$table];
        // return parent::get_orm_class($table);
    }

    /**
     * Create a tag, or update the tag if one already exists for $uuid
     *
     * Note: You CAN delete a search item AFTER a tag has been created, while preserving the tag.
     *
     * @param $uuid the uuid of the item to create a tag for. MUST have a matching item in the Search table. 36 chars max, should be mysql-compatible uuid
     * @param $tag_name the short name of the tag. 64 chars max (shorter is better)
     * @param $tag_type the type of the tag, for filtering. 32 chars max
     * @param $tag_description probably won't actually be used. 512 chars max.
     *
     * @return the newly created (or updated) tag 
     */
    public function create_tag(string $uuid, string $tag_name, string $tag_type, string $tag_description = ''): SearchDb\Tag {
        $search_items = $this->get('search', ['uuid'=>$uuid]);
        if (count($search_items)==0){
            throw new \Exception("Cannot create a tag because no search item exists for uuid `$uuid`");
        }
        $search = $search_items[0];

        $rows = $this->get('tag', ['item_uuid'=>$uuid]);
        if (count($rows)==0){
            $tag = new SearchDb\Tag($this);
        } else {
            $tag = $rows[0];
        }


        $tag->tag_name = $tag_name;
        $tag->tag_description = $tag_description;
        $tag->tag_type = $tag_type;
        $tag->search_id = $search->id;
        $tag->item_uuid = $uuid;


        $tag->save();
        $tag->refresh();

        $search->add_tag($tag);

        return $tag;
    }

    /**
     * Add or UPDATE a searchable item.
     * @param $uuid the uuid of the item, 36 chars max, typically a mysql-compliant uuid
     * @param $type the type of item being indexed. Used to filter results.
     * @param $title item's title, 512 chars max
     * @param $body the full string content to make searchable. You should remove unnecessary data (like html tags) first.
     * @param $uri url to view the item, 512 chars max (only needs path if search is on the same server)
     */
    public function add_searchable(string $uuid, string $type, string $title, string $body, string $uri, \DateTime $published_at = new \DateTime()): SearchDb\Search {
        $rows = $this->get('search', ['uuid'=>$uuid]);
        if (count($rows)==0){
            $search = new SearchDb\Search($this);
            $search->uuid = $uuid;
        } else {
            $search = $rows[0];
        }
        $search->title = $title;
        $search->summary = substr($body, 0,1000);
        $search->body = $body;
        $search->type = $type;
        $search->uri = $uri;
        $search->published_at = $published_at;
        $search->save();
        $search->refresh();

        return $search;
    }

    /**
     * Delete an item from the search index by its uuid
     *
     * @param $uuid the uuid of the item to remove from search.
     * @return true if deleted, false if there was nothing to delete
     */
    public function delete_from_search(string $uuid): bool {
        $orms = $this->get('search', ['uuid'=>$uuid]);
        if (count($orms)==0){
            return false;
        }
        $orms[0]->delete();
        return true;
    }

    /**
     * Delete a tag associated with an item having the given uuid. Does NOT delete tagged_by entries
     *
     * @param $uuid the uuid of the item the tag(s) may be referencing.
     * @return true if deleted, false if there was nothing to delete
     */
    public function delete_from_tags(string $uuid): bool {
        $orms = $this->get('tag', ['item_uuid'=>$uuid]);
        if (count($orms)==0){
            return false;
        }
        foreach ($orms as $orm){
            $orm->delete();
        }
        return true;
    }

    /**
     * Delete all `tagged_by` entries for the given item.
     *
     * @param $item_uuid the uuid of the item to remove tags from.
     * @return true if deleted, false if there was nothing to delete
     */
    public function delete_tagged_by(string $item_uuid): void {
        $this->exec('tagged_by.delete_by_item', ['item_uuid'=>$item_uuid]);
    }

    /**
     * Get search results as an array of Search orms.
     *
     * @param $search_term the query string.
     * @param $type array<int index, string type> the type of item to search for, or empty array to search all types
     * @param $tags array<int index, int tag_id> array of tag ids to search for
     *
     * @return array<int index, \DecaturVote\SearchDb\Search>
     */
    public function search(string $search_term, array $types = [], array $tag_ids = [], string $limit='LIMIT 0,30', ?int &$total_row_count = null): array {
        $sql = $this->build_search_query($search_term, $types, $tag_ids, $limit);
        $rows = $this->sql_query($sql);
        if (count($rows)>0)$total_row_count = $rows[0]['total_row_count'];
        $items = [];
        foreach ($rows as $r){
            $items[] = $this->row_to_orm('search', $r);
        }
        return $items;
    }

    /**
     * Get a feed of search results as an array of Search orms, ordered by datetime
     *
     * @param $search_term the query string.
     * @param $type array<int index, string type> the type of item to search for, or empty array to search all types
     * @param $tags array<int index, int tag_id> array of tag ids to search for
     *
     * @return array<int index, \DecaturVote\SearchDb\Search>
     */
    public function feed(string $search_term, array $types = [], array $tag_ids = [], string $limit='LIMIT 0,30', ?int &$total_row_count = null): array {

        $tag_ids = array_map(function($id){return (int)$id;}, $tag_ids);

        $where_types = '';
        if (count($types)>0){
            $types = array_map([$this->pdo, 'quote'], $types);
            $where_types = 'AND s.`type` IN ('. implode(',', $types).')';
        }
        $sql = '';
        if (count($tag_ids)==0){
            $sql = $this->build_query('feed.no_tags', ['query'=>$search_term, 'where_types'=>$where_types, 'limit'=>$limit]);
            // $items = $this->query('search', 'search', ['query'=>$search_term, 'where_types'=>$where_types]);
        } else {
            $tag_id_list = implode(', ',$tag_ids);
                // $items = $this->query
            $sql = 
                $this->build_query('feed.with_tags',
                [   
                    'query'=>$search_term,
                    'tag_ids'=>$tag_id_list,
                    'where_types'=>$where_types,
                    'limit'=>$limit,
                ]
            );
        }




        $rows = $this->sql_query($sql);
        if (count($rows)>0)$total_row_count = $rows[0]['total_row_count'];
        $items = [];
        foreach ($rows as $r){
            $items[] = $this->row_to_orm('search', $r);
        }
        return $items;
    }

    /**
     * Get search results as an array of Search orms.
     *
     * @param $search_term the query string.
     * @param $type array<int index, string type> the type of item to search for, or empty array to search all types
     * @param $tags array<int index, int tag_id> array of tag ids to search for
     *
     * @return array<int index, \DecaturVote\SearchDb\Search>
     */
    public function tags_from_search(string $search_term, array $types = [], array $tag_ids = []): array {
        $search_sql = $this->build_search_query($search_term, $types, $tag_ids, '');
        $search_sql = substr(trim($search_sql),0,-1); // remove the semicolon at the end

        $tags_sql = $this->build_query("tag.get_tags_within_search", ['search_sql'=>$search_sql]);

// echo "\n\n\n-----------\n\n";
        // echo $tags_sql;
// echo "\n\n\n-----------\n\n";
        $tags = $this->query('tag', 'get_tags_within_search', [
                'search_sql'=>$search_sql,
            ]
        );
        return $tags;
    }

    /**
     * Get search results as an array of Search orms.
     *
     * @param $search_term the query string.
     * @param $type array<int index, string type> the type of item to search for, or empty array to search all types
     * @param $tags array<int index, int tag_id> array of tag ids to search for
     *
     * @return array<int index, \DecaturVote\SearchDb\Search>
     */
    public function types_from_search(string $search_term, array $types = [], array $tag_ids = []): array {
        $search_sql = $this->build_search_query($search_term, $types, $tag_ids, '');
        $search_sql = substr(trim($search_sql),0,-1); // remove the semicolon at the end

        // $types_sql = $this->build_query("search.types_from_search", ['search_sql'=>$search_sql]);

// echo "\n\n\n-----------\n\n";
        // echo $tags_sql;
// echo "\n\n\n-----------\n\n";
        $types = $this->query_rows('search.types_from_search', ['search_sql'=>$search_sql]);
        return $types;
    }

    /**
     * @see search() <- uses the same params
     *
     * @limit sql limit text
     */
    public function build_search_query(string $search_term, array $types = [], array $tag_ids = [], string $limit = 'LIMIT 0,30'){

        $tag_ids = array_map(function($id){return (int)$id;}, $tag_ids);

        $where_types = '';
        if (count($types)>0){
            $types = array_map([$this->pdo, 'quote'], $types);
            $where_types = 'AND s.`type` IN ('. implode(',', $types).')';
        }
        if (count($tag_ids)==0){
            return $this->build_query('search.search', ['query'=>$search_term, 'where_types'=>$where_types, 'limit'=>$limit]);
            // $items = $this->query('search', 'search', ['query'=>$search_term, 'where_types'=>$where_types]);
        } else {
            $tag_id_list = implode(', ',$tag_ids);
                // $items = $this->query
            return 
                $this->build_query('search.search_with_tags',
                [   
                    'query'=>$search_term,
                    'tag_ids'=>$tag_id_list,
                    'where_types'=>$where_types,
                    'limit'=>$limit,
                ]
            );
        }
    }


    /**
     * Get Tags where `id` is in the list of ids.
     *
     * @param $int_ids array<int index, int tag_id> array of tag ids
     * @return array<int index, DecaturVote\SearchDb\Tag tag> array of tag objects
     */
    public function get_tags_in(array $int_ids): array {
        if (count($int_ids)==0)return [];

        $ids = [];
        foreach ($int_ids as $index=>$id){
            $key = ':id_'.((int)$index);
            $ids[$key] = (int)$id;
        }
        $id_keys = implode(', ', array_keys($ids));

        $query = "SELECT * FROM `tag` WHERE `id` IN ($id_keys)";
        //echo $query;
        //exit;

        $tag_rows = $this->sql_query($query, $ids, 'Tag');
        $tags = array_map(function($row){return $this->row_to_orm('tag', $row);}, $tag_rows);

        return $tags;
    }

    /**
     * Get Tags where `tag_uuid` is in the list of uuids.
     *
     * @param $uuids array<int index, string tag_uuid> array of tag uuids
     * @return array<int index, DecaturVote\SearchDb\Tag tag> array of tag objects
     */
    public function get_tags_by_uuids(array $uuids): array {
        if (count($uuids)==0)return [];

        $ids = [];
        foreach ($uuids as $index=>$uuid){
            $key = ':id_'.((int)$index);
            $ids[$key] = $uuid;
        }
        $id_keys = implode(', ', array_keys($ids));

        $query = "SELECT * FROM `tag` WHERE BIN_TO_UUID(`tag_uuid`) IN ($id_keys)";
        //echo $query;
        //exit;

        $tag_rows = $this->sql_query($query, $ids, 'Tag');
        $tags = array_map(function($row){return $this->row_to_orm('tag', $row);}, $tag_rows);

        return $tags;
    }

    /**
     * Get Tags where `item_uuid` is in the list of item_uuids.
     *
     * @param $item_uuids array<int index, string item_uuid> array of item uuids
     * @return array<int index, DecaturVote\SearchDb\Tag tag> array of tag objects
     */
    public function get_tags_by_item_uuids(array $item_uuids): array {
        if (count($item_uuids)==0)return [];

        $ids = [];
        foreach ($item_uuids as $index=>$uuid){
            $key = ':id_'.((int)$index);
            $ids[$key] = $uuid;
        }
        $id_keys = implode(', ', array_keys($ids));

        $query = "SELECT * FROM `tag` WHERE `item_uuid` IN ($id_keys)";
        //echo $query;
        //exit;

        $tag_rows = $this->sql_query($query, $ids, 'Tag');
        $tags = array_map(function($row){return $this->row_to_orm('tag', $row);}, $tag_rows);

        return $tags;
    }

    /**
     * Get the tag that is uniquely identified by the given $item_uuid.
     *
     * @param $item_uuid string uuid of an item that a tag is uniquely identified by. This is the item's original UUID.
     * @return \DecaturVote\SearchDb\Tag or null if none found. If multiple tags are found, an exception is thrown.
     */
    public function get_item_tag(string $item_uuid): ?\DecaturVote\SearchDb\Tag {
        $tags = $this->query("tag", "get_item_tag", ['item_uuid'=>$item_uuid]);
        if (count($tags) > 1){
            throw new \Exception("There are multiple tags created for the item with uuid '$item_uuid'");
        } else if (count($tags) == 0)return null;
        return $tags[0];
    }

    /**
     * Get an array of search items that are tagged by the given Tag uuid. 
     * Use `$db->get_tags_on_item($item_uuid)` to get Tags instead of Search items.
     *
     * In some applications, a tag & tagged_by entry may persist even if the search item has been deleted. 
     *
     * @param $tag_uuid the uuid of a tag
     * @return array<int index, \DecaturVote\SearchDb\Search search_item> array of search items tagged by the given tag. May be empty.
     */
    public function get_items_tagged_by(string $tag_uuid): array {
        $search_items = $this->query('search', 'items_tagged_by_uuid', ['tag_uuid'=>$tag_uuid]);
        return $search_items;
    }

    /**
     * Tag an existing search item.
     *
     * @param $item_uuid string uuid of the source item 
     * @param $tag_uuid string uuid of the tag that this $item_uuid is tagged by
     *
     * @return void
     */
    public function tag_item(string $item_uuid, string $tag_uuid){
        $search_item = $this->get_search_item($item_uuid);
        $search_item->add_tag_by_uuid($tag_uuid);
    }

    /**
     * Get an existing Search Item from a source item's uuid
     *
     * @param $item_uuid string the uuid of your source item.
     * @return \DecaturVote\SearchDb\Search or null if no search item exists
     * @throws Exception if multiple search items are returned
     */
    public function get_search_item(string $item_uuid): ?\DecaturVote\SearchDb\Search {
        $search_items = $this->query("search","by_item_uuid", ['item_uuid'=>$item_uuid]);
        if (count($search_items) > 1){
            throw new \Exception("Multiple search items were returned for source item uuid '$item_uuid'");
        }
        return $search_items[0] ?? null;
    }

    /**
     * Get an array of tags that the given item is tagged by
     *
     * @param $item_uuid string the item that is tagged by tags
     * @return array<int index, \DecaturVote\SearchDb\Tag tag> array of tags. may be an empty array
     */
    public function get_tags_on_item(string $item_uuid): array {
        $tag_items = $this->query('tag', 'by_item_uuid', ['item_uuid'=>$item_uuid]);
        return $tag_items;
    }
}
