-- @query(create, -- END)
DROP TABLE IF EXISTS `tagged_by`;

CREATE TABLE IF NOT EXISTS `tagged_by` (
    `id` int unsigned PRIMARY KEY AUTO_INCREMENT,
    `tag_id` int, -- The tag that is added to the item referenced by the item_uuid & search_id
    `item_uuid` varchar(36), -- The uuid of the item that is tagged by tag_id
    `search_id` int, -- the search id of the item that is tagged by tag_id . search.uuid == this.item_uuid

    `created_at` DATETIME DEFAULT NOW(),
    `updated_at` DATETIME DEFAULT NOW() ON UPDATE NOW()

);
-- END

-- @query(by_item_uuid)
SELECT * FROM tagged_by WHERE item_uuid LIKE :item_uuid;

-- @query(delete_by_item)
DELETE FROM tagged_by WHERE item_uuid LIKE :item_uuid;

-- @query(get_num_items_that_tag_item_uuid, -- END)
SELECT COUNT(*) as num_items_that_tag_this_item 
FROM tagged_by
JOIN `tag` 
    ON `tag`.id = tagged_by.tag_id 
WHERE
    `tag`.item_uuid = :item_uuid 
;
-- END
