-- @query(no_tags)
SELECT id, `type`, uuid, title, summary, uri, updated_at, published_at, created_at, COUNT(*) OVER() AS total_row_count  FROM search s
        WHERE 
            ( `title` LIKE CONCAT('%',:query,'%')
                OR `body` LIKE CONCAT('%',:query,'%')
            ) 
            {$where_types}
        ORDER BY 
            `published_at` DESC
        {$limit}
;

-- @query(with_tags)
SELECT s.id, s.type, s.uuid, s.title, s.summary, s.uri, s.updated_at, s.published_at, s.created_at, COUNT(*) OVER() AS total_row_count FROM search s
        JOIN `tagged_by` tb
            ON tb.search_id = s.id
        WHERE 
            tb.tag_id IN ({$tag_ids})
            AND (
               s.`title` LIKE CONCAT('%',:query,'%')
                OR s.`body` LIKE CONCAT('%',:query,'%')
            )
            {$where_types}
        ORDER BY 
            s.`published_at` DESC
        {$limit}
;
