-- @query(create, -- END)
DROP TABLE IF EXISTS `search`;

CREATE TABLE IF NOT EXISTS `search` (

    `id` int unsigned PRIMARY KEY AUTO_INCREMENT,

    `uuid` varchar(36), -- uuid of the item that this search came from

    `title` VARCHAR(512),
    `type` VARCHAR(32),
    `summary` VARCHAR(1024),
    `body` TEXT,
    `uri` VARCHAR(512),

    `published_at` DATETIME DEFAULT NOW(), -- original publish date of the original item
    `created_at` DATETIME DEFAULT NOW(),
    `updated_at` DATETIME DEFAULT NOW() ON UPDATE NOW(),

    UNIQUE(`uuid`)

);
-- END

-- @query(search)
SELECT id, `type`, uuid, title, summary, uri, updated_at, published_at, created_at, COUNT(*) OVER() AS total_row_count  FROM search s
        WHERE 
            ( `title` LIKE CONCAT('%',:query,'%')
                OR `body` LIKE CONCAT('%',:query,'%')
            ) 
            {$where_types}
        ORDER BY 
            CASE
                WHEN `title` LIKE :query THEN 1
                WHEN `title` LIKE CONCAT(:query,'%') THEN 2
                WHEN `title` LIKE CONCAT('%',:query) THEN 3
                WHEN `title` LIKE CONCAT('%',:query,'%') THEN 4

                WHEN `body` LIKE :query THEN 5
                WHEN `body` LIKE CONCAT(:query,'%') THEN 6
                WHEN `body` LIKE CONCAT('%',:query) THEN 7
                WHEN `body` LIKE CONCAT('%',:query,'%') THEN 8

                ELSE 9
            END ASC,
            `published_at` DESC
        {$limit}
;

-- @query(search_with_tags)
SELECT s.id, s.type, s.uuid, s.title, s.summary, s.uri, s.updated_at, s.published_at, s.created_at, COUNT(*) OVER() AS total_row_count FROM search s
        JOIN `tagged_by` tb
            ON tb.search_id = s.id
        WHERE 
            tb.tag_id IN ({$tag_ids})
            AND (
               s.`title` LIKE CONCAT('%',:query,'%')
                OR s.`body` LIKE CONCAT('%',:query,'%')
            )
            {$where_types}
        GROUP BY  s.id
        ORDER BY 
            CASE
                WHEN s.`title` LIKE :query THEN 1
                WHEN s.`title` LIKE CONCAT(:query,'%') THEN 2
                WHEN s.`title` LIKE CONCAT('%',:query) THEN 3
                WHEN s.`title` LIKE CONCAT('%',:query,'%') THEN 4

                WHEN `body` LIKE :query THEN 5
                WHEN `body` LIKE CONCAT(:query,'%') THEN 6
                WHEN `body` LIKE CONCAT('%',:query) THEN 7
                WHEN `body` LIKE CONCAT('%',:query,'%') THEN 8

                ELSE 9
            END ASC,
            s.`published_at` DESC
        {$limit}
;


-- @query(all_types)
SELECT DISTINCT s.type, COUNT(*) as c FROM `search` s
    GROUP BY s.type
    ORDER BY c DESC
    ;

-- @query(types_from_search)
SELECT DISTINCT DISTINCT s.type, COUNT(sub.id) c FROM `search` s
    LEFT JOIN ({$search_sql}) sub
        ON sub.id = s.id
    GROUP BY s.type
    ORDER BY c DESC
    ;
--  SELECT DISTINCT s.type, COUNT(*) as c FROM ({$search_sql}) 

-- @query(by_item_uuid)
SELECT search.* FROM search
WHERE search.uuid = :item_uuid
;

-- @query(items_tagged_by_uuid)
SELECT search.* FROM tag 
JOIN tagged_by
    ON tagged_by.tag_id = tag.id
JOIN search
    ON tagged_by.search_id = search.id
WHERE tag.tag_uuid = UUID_TO_BIN( :tag_uuid )
;

