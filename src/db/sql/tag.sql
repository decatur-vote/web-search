-- @query(create, -- END)
DROP TABLE IF EXISTS `tag`;

CREATE TABLE IF NOT EXISTS `tag` (
    `id` int unsigned PRIMARY KEY AUTO_INCREMENT,
    `tag_uuid` BINARY(16) DEFAULT ( UUID_TO_BIN( UUID() ) ) NOT NULL, -- probably binary
    `item_uuid` VARCHAR(36), -- all tags are created with an item as a reference
    `search_id` int, -- referencing the search table - this COULD be how we load a URI, title, and description. 
                    -- If you're viewing a tag, then 'this tag references [Title](uri)'
                    -- `item_uuid` COULD be used for that, but `search_id` will provide faster JOINs
    `tag_name` varchar(64), -- the shorthand for the tag, like `john-smith` or `decatur-city-council`
    `tag_description` varchar(512), -- short description of the tag. This is NOT intended to be a description of the referenced item.
    `tag_type` varchar(32), -- to distinguish topics, articles, outside-sources, etc. Ex: Default search only finds search items with the tag type 'topic' or 'article', but can be expanded to search for sources, tidbits, herald & review articles, and so-on.

    `created_at` DATETIME DEFAULT NOW(),
    `updated_at` DATETIME DEFAULT NOW() ON UPDATE NOW(),

    UNIQUE(`tag_uuid`),
    UNIQUE(`item_uuid`)

);
-- END

-- @query(get_item_tag)
-- get a tag uniquely identified by the given item_uuid
SELECT tag.* FROM `tag`
    WHERE tag.item_uuid = :item_uuid
    ;

-- @query(by_item_uuid)
-- get tags attached to an item
SELECT DISTINCT tag.* FROM `tag` 
    JOIN `tagged_by` tb
        ON tb.tag_id = tag.id
    WHERE
        tb.item_uuid LIKE :item_uuid
    ;
    

-- @query(get_all)
SELECT * FROM `tag`;

-- @query(by_name)
SELECT * FROM `tag` WHERE `tag_name` LIKE :name;

-- @query(lookup_tags)
SELECT * FROM `tag` 
    WHERE `tag_name` LIKE CONCAT('%', :name, '%');
        OR `tag_description` LIKE CONCAT('%', :name, '%')
        ORDER BY 
            CASE
                WHEN `tag_name` LIKE :name THEN 1
                WHEN `tag_name` LIKE CONCAT(:name,'%') THEN 2
                WHEN `tag_name` LIKE CONCAT('%',:name) THEN 3
                WHEN `tag_name` LIKE CONCAT('%',:name,'%') THEN 4

                WHEN `tag_description` LIKE :name THEN 5
                WHEN `tag_description` LIKE CONCAT(:name ,'%') THEN 6
                WHEN `tag_description` LIKE CONCAT('%',:name ) THEN 7
                WHEN `tag_description` LIKE CONCAT('%',:name ,'%') THEN 8

                ELSE 9
            END ASC
;

-- @query(get_tags_within_search)
SELECT t.*, count(t.id) AS item_count FROM tagged_by tb
    JOIN tag t
        ON t.id = tb.tag_id
    JOIN ({$search_sql}) AS s
        ON tb.search_id = s.id

    GROUP BY t.id
    ORDER BY `item_count` DESC
    ;
