<?php

namespace DecaturVote\SearchDb;

/**
 * A single, searchable item.
 */
class Search extends \Tlf\BigOrm {

    public string $table = 'search';

    /** int id of the search row */
    public int $id;
    /** 36 char mysql-compliant string uuid of the item you're indexing*/
    public string $uuid;
    /** 32 char item type, for built-in filtering. Examples: article, blog-post, office, person, place*/
    public string $type;
    /** String title to display in search results & use for querying. 512 chars max */
    public string $title;
    /** String summary to display in search results & use for querying. 1024 chars max */
    public string $summary;
    /** Full string body. Should NOT be returned from database during queries. */
    public string $body;
    /** String uri to go to when item is clicked in search results. 512 chars max */
    public string $uri;

    /** When the source item was published */
    public \DateTime $published_at;

    /** When the search item was created */
    public \DateTime $created_at;
    /** When the search item was last updated*/
    public ?\DateTime $updated_at = null;

    /**
     * Get a user-friendly string representation the item's date (published_at ?? updated_at ?? created_at)
     * As of june 2, 2023 format is `Y-m-d g:i a`
     *
     * @return user-friendly date-time string. 
     */
    public function getUpdatedAt(): string {
        $dt = $this->getDateTime();

        return $dt->format("Y-m-d g:i a");
    }

    public function getDateTime(): \DateTime {
        $dt = $this->published_at
            ?? $this->updated_at
            ?? $this->created_at
            ;
        return $dt;
    }

    public function set_from_db(array $row){
        $this->id = $row['id'];
        $this->uuid = $row['uuid'];//$this->bin_to_uuid($row['diff_uuid']);
        $this->title = $row['title'];
        $this->summary = $row['summary'];
        if (isset($row['body']))$this->body = $row['body'];
        $this->uri = $row['uri'];
        $this->type = $row['type'];

        $this->created_at = $this->str_to_datetime($row['created_at']);
        $this->published_at = isset($row['published_at']) ? $this->str_to_datetime($row['published_at']) : $this->created_at;
        $this->updated_at = empty($row['updated_at']) ? null : $this->str_to_datetime($row['updated_at']);
    
    }

    public function get_db_row(): array {
        $row = [
            'uuid'=>$this->uuid,
            'title'=>$this->title,
            'summary'=>$this->summary,
            'uri'=>$this->uri,
            'type'=>$this->type,
        ];
        if (isset($this->id))$row['id'] = $this->id;
        if (isset($this->created_at))$row['created_at'] = $this->datetime_to_str($this->created_at);
        if (isset($this->updated_at))$row['updated_at'] = $this->datetime_to_str($this->updated_at);
        if (isset($this->published_at))$row['published_at'] = $this->datetime_to_str($this->published_at);
        if (isset($this->body))$row['body'] = $this->body;

        return $row;
    }

    /**
     * Add a tag to this search item, if it is not already added to it. 
     *
     * @param $tag the tag to add
     * @return void
     */
    public function add_tag(Tag $tag): void{
        if (!$tag->is_saved()){
                    throw new \Exception("Cannot add a tag to a search item if the tag has not been saved. Call save() & refresh() on the tag before adding it to a search item.");
        }

        $this->add_tag_by_id($tag->id);

    }

    /**
     * Same as add_tag, but takes int tag_id instead of an instantiated Tag object.
     */
    public function add_tag_by_id(int $tag_id): void {
        if (!$this->is_saved()){
            throw new \Exception("Cannot add a tag to a Search item that has not been saved. You should save() & refresh() before adding a tag.");
        } 

        $this->db->insert('tagged_by',
            ['tag_id'=>$tag_id,
            'item_uuid'=>$this->uuid,
            'search_id'=>$this->id
            ]
        );

    }

    /**
     * Same as add_tag, but takes string tag_uuid instead of an instantiated Tag object.
     */
    public function add_tag_by_uuid(string $uuid): void {
        if (!$this->is_saved()){
            throw new \Exception("Cannot add a tag to a Search item that has not been saved. You should save() & refresh() before adding a tag.");
        } 

        $rows = $this->db->sql_query("SELECT `id` from `tag` WHERE `tag_uuid` = UUID_TO_BIN(:tag_uuid)", ['tag_uuid'=>$uuid]);
        if (count($rows)==0){
            throw new \Exception("Tag does not exist with uuid '$uuid'");
        }
        $tag_id = $rows[0]['id'];

        $this->db->insert('tagged_by',
            ['tag_id'=>$tag_id,
            'item_uuid'=>$this->uuid,
            'search_id'=>$this->id
            ]
        );

    }

    /**
     * Mark this search item as 'tagged_by' the tag uniquely identified by the $item_uuid
     *
     * @param $item_uuid string uuid of the item that uniquely identifies the tag to add
     * @return void
     */
    public function add_tag_by_item_uuid(string $item_uuid): void {
        if (!$this->is_saved()){
            throw new \Exception("Cannot add a tag to a Search item that has not been saved. You should save() & refresh() before adding a tag.");
        } 

        $rows = $this->db->sql_query("SELECT `id` from `tag` WHERE `item_uuid` = :item_uuid", ['item_uuid'=>$item_uuid]);
        if (count($rows)==0){
            throw new \Exception("Tag does not exist with item_uuid '$item_uuid'");
        }
        $tag_id = $rows[0]['id'];

        $this->db->insert('tagged_by',
            ['tag_id'=>$tag_id,
            'item_uuid'=>$this->uuid,
            'search_id'=>$this->id
            ]
        );

    }

}
