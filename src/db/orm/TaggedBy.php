<?php

namespace DecaturVote\SearchDb;

/**
 * A tag, which points to a search item as its single source of truth and can be added to other search items
 */
class TaggedBy extends \Tlf\BigOrm {

    public string $table = 'search';

    /** int id of the search row */
    public int $id;
    public int $tag_id;
    /** 36 char mysql-compliant string uuid of the item you're indexing*/
    public string $item_uuid;
    /** int id of a Search item. */
    public int $search_id;


    //[>* 36 char mysql-compliant string uuid of the item you're indexing<]
    //public string $tag_uuid;

    //[>* The short name for the tag, typically how it will be displayed publicly. 64 chars. <]
    //public string $tag_name;
    //[>* Description of the tag (not the item it references). May be removed?? 512 chars <]
    //public string $tag_description;
    //[>* 32 char tag type, for distinguising the type of the item this tag references. May distinguish topics, articles, outside-source, and so-on <]
    //public string $tag_type;

    /** When the search item was created */
    public \DateTime $created_at;
    /** When the search item was last updated*/
    public ?\DateTime $updated_at;

    public function set_from_db(array $row){
        //var_dump($row);
        //exit;
        //$this->set_props_from_array($row,'id','search_id','item_uuid','tag_name','tag_description','tag_type');
        $this->set_props_from_array($row,'id','tag_id', 'item_uuid', 'search_id');

        //$this->tag_uuid = $this->bin_to_uuid($row['tag_uuid']);
        $this->created_at = $this->str_to_datetime($row['created_at']);
        $this->updated_at = empty($row['updated_at']) ? null : $this->str_to_datetime($row['updated_at']);
    }

    public function get_db_row(): array {
        $row = $this->props_to_array('id','search_id','item_uuid','tag_name','tag_description','tag_type');

        $row['tag_uuid'] = $this->uuid_to_bin($this->tag_uuid);
        $row['created_at'] = $this->datetime_to_str($this->created_at);
        
        if (isset($this->updated_at)&&!is_null($this->updated_at))$row['updated_at'] = $this->datetime_to_str($this->updated_at);

        return $row;
    }


}
