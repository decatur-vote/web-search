<?php

namespace DecaturVote\SearchDb;

/**
 * A tag, which points to a search item as its single source of truth and can be added to other search items
 */
class Tag extends \Tlf\BigOrm {

    public string $table = 'tag';

    /** int id of the tag row */
    public int $id;
    /** int id of a Search item. */
    public ?int $search_id;
    /** 36 char mysql-compliant string uuid of the item you're indexing*/
    public ?string $item_uuid;
    /** 36 char mysql-compliant string uuid of the tag's uuid */
    public string $tag_uuid;

    /** The short name for the tag, typically how it will be displayed publicly. 64 chars. */
    public string $tag_name;
    /** Description of the tag (not the item it references). May be removed?? 512 chars */
    public string $tag_description;
    /** 32 char tag type, for distinguising the type of the item this tag references. May distinguish topics, articles, outside-source, and so-on */
    public string $tag_type;

    /** When the search item was created */
    public \DateTime $created_at;
    /** When the search item was last updated*/
    public ?\DateTime $updated_at;

    /** The number of items matching this tag in the current context 
     * Context typically being a search term & list of other tags to refine search terms) 
     * May be null, as it is a calculated value only generated in certain contexts (like during search)
     */
    public ?int $item_count = null;

    /**
     * If tag already exists for given name, return that tag. Otherwise, create new tag and return it.
     *
     * You must subsequently call `$tag->save()` to store in db if `$tag->is_saved()` returns false, meaning it is a new tag.
     *
     * @return Tag instance
     */
    static public function tag_from_name(\DecaturVote\SearchDb $db, string $name): Tag {
        $tag = $db->get('tag', ['tag_name'=>$name])[0] ?? null; 
        if ($tag!=null)return $tag;

        $tag = new Tag($db);
        $tag->tag_name = $name;
        $tag->tag_type = 'created_from_name';
        $tag->tag_description = '-no description-';

        return $tag;
    }

    public function set_from_db(array $row){
        $this->set_props_from_array($row,'id','search_id','item_uuid','tag_name','tag_description','tag_type');

        $this->tag_uuid = $this->bin_to_uuid($row['tag_uuid']);
        $this->created_at = $this->str_to_datetime($row['created_at']);
        $this->updated_at = empty($row['updated_at']) ? null : $this->str_to_datetime($row['updated_at']);

        if (isset($row['item_count']))$this->item_count = $row['item_count'];
    }

    public function get_db_row(): array {
        $row = $this->props_to_array('id','search_id','item_uuid','tag_name','tag_description','tag_type');

        if (isset($this->tag_uuid))$row['tag_uuid'] = $this->uuid_to_bin($this->tag_uuid);
        if (isset($this->created_at))$row['created_at'] = $this->datetime_to_str($this->created_at);
        
        if (isset($this->updated_at)&&!is_null($this->updated_at))$row['updated_at'] = $this->datetime_to_str($this->updated_at);

        if (!is_null($this->item_count))$row['item_count'] = $this->item_count;

        if (strlen($row['tag_name']) > 64){
            $row['tag_name'] = substr($row['tag_name'], 0, 64);
        }

        return $row;
    }


}
