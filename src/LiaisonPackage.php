<?php

namespace DecaturVote\Search;

class LiaisonPackage extends \Lia\Package\Server {

    public \DecaturVote\Search\IntegrationInterface $integration;

    public \PDO $pdo;

    public \Tlf\LilDb $ldb;
    public \Tlf\BigDb $bigdb;

    public function __construct(\Lia $lia, \DecaturVote\Search\IntegrationInterface $integration){
        $root = dirname(__DIR__,1);
        parent::__construct($lia, 'decatur-vote:web-search',$root.'/src/','/');


        $this->pdo = $integration->getPdo();
        //$this->ldb = new \Tlf\LilDb($this->pdo);
        $this->bigdb = new \DecaturVote\SearchDb($this->pdo);

        $this->integration = $integration;

    }


}
