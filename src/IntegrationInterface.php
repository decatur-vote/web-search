<?php

namespace DecaturVote\Search;

interface IntegrationInterface {
    /** get a valid PDO instance */
    public function getPdo(): \PDO;

    /** Get an array of RSS channel information, per the 2.0 RSS spec. See https://validator.w3.org/feed/docs/rss2.html */
    public function getRssChannel(): array;

    /** get a schem & host (like `https://example.com`) to prepend to any relative urls in your search items, primarily for rss */
    public function getHostWebsite(): string;

    /** called when the page view is loaded. You should use this to add your own styles. 
     *
     * @param $lia the liaison instance
     */
    public function page_will_display(\Lia $lia): void;

    /**
     * Called when the view `search/tag-feed` is loaded. Use this to add styles.
     */
    public function tag_feed_will_display(\Lia $lia): void;

    public function add_tag_will_display(\Lia $lia): void;

    /** 
     * Check if tag creation is allowed for current user.
     *
     * @param $lia liaison instance
     * @param $tag The tag that will be created, if allowed
     *
     * @return true if creation allowed, false if not.
     */
    public function can_create_tag(\Lia $lia, \DecaturVote\SearchDb\Tag $tag): bool;
}
