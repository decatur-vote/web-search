<?php

namespace DecaturVote\Search;

/**
 * Command Line class for database management
 */
class BigDbCli extends \Tlf\BigDb\LibraryCli {


    public function get_root_dir(): string {
        return dirname(__DIR__);
    }


}
