<?php
/**
 * GET /feed/{tag-name}/
 *
 * @param $tag the string name of a tag
 */

$db = new \DecaturVote\SearchDb($package->integration->getPdo());

$tag = strip_tags($tag);

$tag_row = $db->query_rows('tag.by_name', ['name'=>$tag])[0]??null;

if ($tag_row==null){
    echo '<h1>Invalid Tag</h1>';
    echo "<p>Tag '$tag' is not valid.</p>";
    return;
}

$format = $_GET['format']??null;
$_GET = [
    'tags'=>[$tag_row['id']]
];
if ($format!=null)$_GET['format'] = $format;

require(__DIR__.'/../feed.php');
