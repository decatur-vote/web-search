<?php

namespace DecaturVote\Search;

class Paginator {

    public int $current_page;
    protected int $page_size;
    protected int $num_results;
    protected string $base_url;
    protected array $params;

    protected int $num_pages;

    public function __construct(int $current_page, int $page_size, int $num_results, string $base_url, array $params){
        $this->current_page = $current_page;
        $this->page_size = $page_size;

        $this->base_url = $base_url;
        $this->params = $params;

        $this->setNumResults($num_results);
    }

    /**
     * Set the size of the resultset
     * @param $num_results the number of rows available
     */
    public function setNumResults(int $num_results){
        $this->num_pages = (int)ceil($num_results/$this->page_size);
        $this->num_results = $num_results;
    }


    /**
     * Get the sql limit
     *
     * @return a string like `LIMIT 30, 10`
     */
    public function get_limit(): string {
        $page = (int)($this->current_page??0);
        $page_size = $this->page_size;
        $start = $page * $page_size;

        return "LIMIT $start,$page_size";
    }

    public function get_links(int $num_links=11): array {

        $num_pages = $this->num_pages;
        $page_links = [];
        $base_url = $this->base_url;
        $params = $this->params;
        unset($params['format']);
        $page_number = $this->current_page;

        if ($num_pages==0&&$page_number!=0){
            unset($params['p']);
            $page_links[1] = $base_url.'?'.http_build_query($params);
        } else {


            $this->get_range($page_number, $num_pages, $num_links, $start_page, $end_page);
            if ($start_page==$end_page)return [];
            for ($i=$start_page;$i<=$end_page;$i++){
                $params['p'] = $i;
                $page_links[$i+1] = $base_url.'?'.http_build_query($params);
            }
        }

        return $page_links;
    }

    /**
     * Generate a pagination range, where we TRY to put the current page directly in the middle of the start page and end page, but we guarantee not to go outside the bounds of start_page>=0 and end_page < num_pages.
     *
     * @param $cur_page the page currently selected - this will be in the middle of start_page and end_page if possible
     * @param $num_pages the total number of pages. (starting at 1)
     * @param $range_size the maximum size of the range. (starting at 1)
     * @param &$start_page the start page will be calculated and set. ALWAYS >= 0. 
     * @param &$end_page the end page will be calculated and set. ALWAYS < $num_pages (end_page is zero-indexed whereas num_pages is one-indexed)
     * 
     * @return void
     */
    public function get_range(int $cur_page, int $num_pages, int $range_size, ?int &$start_page, ?int &$end_page): void {
        $diff = floor($range_size / 2);
        $start_page = $cur_page - $diff;
        $end_page = $cur_page + $diff;
        if ($start_page < 0) {
            $end_page = $end_page + (0 - $start_page);
            $ideal_start = 0;
        }

        if ($end_page >= $num_pages){
            $start_page = $start_page - ($end_page - $num_pages) - 1;
            $end_page = $num_pages - 1;
        }
        if ($start_page < 0)$start_page = 0;
    }



}
