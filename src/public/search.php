<?php 
/**
 * GET /search/ Perform a search. All paramaters are optional.
 *
 * @param string q the query string
 * @param string format=json to get results as a json array with keys 'results', 'types', 'tags', and 'page_links'
 * @param int p page number, 0-based
 * @param int page_size between 1 and 20.
 * @param search_for[] array<int index, string type> where string type is like 'article' or 'office'
 * @param tags[] array<int index, int tag_id> where tag_id is foundd in the 'tags' array of json results
 *
 * EXAMPLE 
 * GET /      <- Filter by search item's type & items with the given tags
 *    ?q=Query&
 *    &search_for[]=type
 *    &search_for[]=type2
 *    &tags[]=int tag_id
 *    &tags[]=int tag_id2
 *    &p=int page_number
 *
 * Include `?format='json'` to get results as json array
 *
 */

$lia->addResourceFile($package->dir('view').'/search/SearchRows.js');
$lia->addResourceFile($package->dir('view').'/search/SearchControls.js');

$query_string = $_GET['q'] ?? '';
$search_for = $_GET['search_for'] ?? [];
$tags = $_GET['tags'] ?? [];

$page = (int)($_GET['p']??0);

$paginator = new \DecaturVote\Search\Paginator($page, 
    $page_size=max(1, min((int)($_GET['page_size']??10),20)),
    $total_row_count=0, 
    parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), 
    $_GET);

$db = new \DecaturVote\SearchDb($package->integration->getPdo());
$items = $db->search($query_string, $search_for, $tags,$paginator->get_limit(), $total_row_count);
$available_tags = $db->tags_from_search($query_string, $search_for);
$available_types = $db->types_from_search($query_string, [], $tags);

$paginator->setNumResults($total_row_count);




// echo "\n\n\n-----------\n\n";
// echo "COUNT! $total_row_count";
// echo "\n\n\n-----------\n\n";
// exit;

if (isset($_GET['format'])
    &&$_GET['format']=='json'){
    echo $lia->view('search/json_results', 
        [ 'search_items'=>$items,
          'tags'=>$available_tags,
          'available_types'=>$available_types,
          'paginator'=>$paginator
        ] 
    );
    exit;
}

echo $lia->view('search/page', 
    [ 
      'search_term'=>strip_tags($query_string),
      'search_items'=>$items,
      'selected_types'=>$search_for,
      'selected_tags'=>$tags,
      'available_tags'=>$available_tags,
      'available_types'=>$available_types,
      'paginator'=>$paginator,
    ] 
);
