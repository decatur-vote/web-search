<?php 
/**
 * GET /feed/ View a feed, all paramaters are optional
 *
 * @param string q the query string
 * @param string format=json to get results as a json array with keys 'results', 'types', 'tags', and 'page_links'
 * @param int p page number, 0-based
 * @param int page_size between 1 and 20.
 * @param search_for[] array<int index, string type> where string type is like 'article' or 'office'
 * @param tags[] array<int index, int tag_id> where tag_id is foundd in the 'tags' array of json results
 *
 * EXAMPLE 
 * GET /      <- Filter by search item's type & items with the given tags
 *    ?q=Query&
 *    &search_for[]=type
 *    &search_for[]=type2
 *    &tags[]=int tag_id
 *    &tags[]=int tag_id2
 *    &p=int page_number
 *
 * Include `?format='json'` to get results as json array
 *
 */

$query_string = $_GET['q'] ?? '';
$search_for = $_GET['search_for'] ?? [];
$tags = $_GET['tags'] ?? [];

$page = (int)($_GET['p']??0);

$paginator = new \DecaturVote\Search\Paginator($page, 
    $page_size=max(1, min((int)($_GET['page_size']??10),20)),
    $total_row_count=0, 
    parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), 
    $_GET);

$db = new \DecaturVote\SearchDb($package->integration->getPdo());
// $items = $db->search($query_string, $search_for, $tags,$paginator->get_limit(), $total_row_count);
if (isset($_GET['format'])&&($_GET['format']=='json'||$_GET['format']=='rss')){
    $limit = 'LIMIT 0, 100';
} else {
    $limit = $paginator->get_limit();
}
$items = $db->feed($query_string, $search_for, $tags,$limit, $total_row_count);
$available_tags = $db->tags_from_search($query_string, $search_for);
$available_types = $db->types_from_search($query_string, [], $tags);

$paginator->setNumResults($total_row_count);




// echo "\n\n\n-----------\n\n";
// echo "COUNT! $total_row_count";
// echo "\n\n\n-----------\n\n";
// exit;

if (isset($_GET['format'])
    &&$_GET['format']=='json'){
    $rows = $db->to_arrays($items);
    $json = json_encode($rows, JSON_PRETTY_PRINT);
    header('Content-type: application/json',true);
    header('Content-Length: '.strlen($json));
    header('Cache-Control: no-cache');
    echo $json;

    exit;
} else if (isset($_GET['format'])
    &&$_GET['format']=='rss'){
    $rss = $lia->view('search/rss', ['items'=>$items, 'channel'=>$package->integration->getRssChannel()]);
    header('Content-type: application/rss+xml',true);
    header('Content-Length: '.strlen($rss));
    header('Cache-Control: no-cache');
    echo $rss;
    exit;
}

echo $lia->view('search/page', 
    [ 
      'search_term'=>strip_tags($query_string),
      'search_items'=>$items,
      'show_controls'=>false,
      'paginator'=>$paginator,
    ] 
);
