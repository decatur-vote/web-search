<?php
/**
 * CREATE a tag on the server
 *
 * @usage GET /search/create-tag/?tag_name=string
 *
 * @param tag_name is the complete name of a tag to create. May be modified by server.
 *
 * @output json SearchTag - SearchTag is an object with int id and string name
 *
 */

$tag_name = str_replace(' ','-',strip_tags($_GET['tag_name'] ?? ''));
$db = new \DecaturVote\SearchDb($package->integration->getPdo());
$tag = \DecaturVote\SearchDb\Tag::tag_from_name($db,$tag_name);

if (!$tag->is_saved() ){
    if ($package->integration->can_create_tag($lia, $tag)){
        $tag->save();
    } else {
        $response = [ 
            'id'=>'-1',
            'name'=> '-error-'
        ];
        echo json_encode($response);
        exit;
    }
}

$response = [
    'id'=>$tag->id,
    'name'=>$tag->tag_name
];

echo json_encode($response);

exit;
