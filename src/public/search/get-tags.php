<?php
/**
 * Get a list of tags as json
 * @usage GET /search/get-tags/?tag_name=string
 *
 * @param tag_name is the partial or complete name of a tag used to query for any matching tags
 *
 * @output json array<int index, object<string name, int id> tag>
 *
 */

$tag_name = str_replace(' ','-',strip_tags($_GET['tag_name'] ?? ''));
$db = new \DecaturVote\SearchDb($package->integration->getPdo());
$tags = $db->query('tag', 'tag.lookup_tags', ['name'=>$tag_name]);


$rows = array_map(
    function (\DecaturVote\SearchDb\Tag $tag){
        return [
            'name'=>$tag->tag_name,
            'id'=>$tag->id,
        ];
    },
    $tags
);

echo json_encode($rows);

exit;
