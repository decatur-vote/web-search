<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/LiaisonPackage.php  
  
# class DecaturVote\Search\LiaisonPackage  
  
See source code at [/src/LiaisonPackage.php](/src/LiaisonPackage.php)  
  
## Constants  
  
## Properties  
- `public \DecaturVote\Search\IntegrationInterface $integration;`   
- `public \PDO $pdo;`   
- `public \Tlf\LilDb $ldb;`   
- `public \Tlf\BigDb $bigdb;`   
  
## Methods   
- `public function __construct(\Lia $lia, \DecaturVote\Search\IntegrationInterface $integration)`   
  
