<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/orm/Search.php  
  
# class DecaturVote\SearchDb\Search  
A single, searchable item.  
See source code at [/src/db/orm/Search.php](/src/db/orm/Search.php)  
  
## Constants  
  
## Properties  
- `public string $table = 'search';`   
- `public int $id;` int id of the search row  
- `public string $uuid;` 36 char mysql-compliant string uuid of the item you're indexing  
- `public string $type;` 32 char item type, for built-in filtering. Examples: article, blog-post, office, person, place  
- `public string $title;` String title to display in search results & use for querying. 512 chars max  
- `public string $summary;` String summary to display in search results & use for querying. 1024 chars max  
- `public string $body;` Full string body. Should NOT be returned from database during queries.  
- `public string $uri;` String uri to go to when item is clicked in search results. 512 chars max  
- `public \DateTime $published_at;` When the source item was published  
- `public \DateTime $created_at;` When the search item was created  
- `public \DateTime $updated_at = null;` When the search item was last updated  
  
## Methods   
- `public function getUpdatedAt(): string` Get a user-friendly string representation the item's date (published_at ?? updated_at ?? created_at)  
As of june 2, 2023 format is `Y-m-d g:i a`  
  
- `public function getDateTime(): \DateTime`   
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
- `public function add_tag(Tag $tag): void` Add a tag to this search item, if it is not already added to it.   
  
- `public function add_tag_by_id(int $tag_id): void` Same as add_tag, but takes int tag_id instead of an instantiated Tag object.  
- `public function add_tag_by_uuid(string $uuid): void` Same as add_tag, but takes string tag_uuid instead of an instantiated Tag object.  
  
