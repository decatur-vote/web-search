<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/orm/TaggedBy.php  
  
# class DecaturVote\SearchDb\TaggedBy  
A tag, which points to a search item as its single source of truth and can be added to other search items  
See source code at [/src/db/orm/TaggedBy.php](/src/db/orm/TaggedBy.php)  
  
## Constants  
  
## Properties  
- `public string $table = 'search';`   
- `public int $id;` int id of the search row  
- `public int $tag_id;`   
- `public string $item_uuid;` 36 char mysql-compliant string uuid of the item you're indexing  
- `public int $search_id;` int id of a Search item.  
- `public \DateTime $created_at;` When the search item was created  
- `public \DateTime $updated_at;` When the search item was last updated  
  
## Methods   
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
  
