<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/orm/Tag.php  
  
# class DecaturVote\SearchDb\Tag  
A tag, which points to a search item as its single source of truth and can be added to other search items  
See source code at [/src/db/orm/Tag.php](/src/db/orm/Tag.php)  
  
## Constants  
  
## Properties  
- `public string $table = 'tag';`   
- `public int $id;` int id of the tag row  
- `public int $search_id;` int id of a Search item.  
- `public string $item_uuid;` 36 char mysql-compliant string uuid of the item you're indexing  
- `public string $tag_uuid;` 36 char mysql-compliant string uuid of the tag's uuid  
- `public string $tag_name;` The short name for the tag, typically how it will be displayed publicly. 64 chars.  
- `public string $tag_description;` Description of the tag (not the item it references). May be removed?? 512 chars  
- `public string $tag_type;` 32 char tag type, for distinguising the type of the item this tag references. May distinguish topics, articles, outside-source, and so-on  
- `public \DateTime $created_at;` When the search item was created  
- `public \DateTime $updated_at;` When the search item was last updated  
- `public int $item_count = null;` The number of items matching this tag in the current context  
Context typically being a search term & list of other tags to refine search terms)   
May be null, as it is a calculated value only generated in certain contexts (like during search)  
  
## Methods   
- `static public function tag_from_name(\DecaturVote\SearchDb $db, string $name): Tag` If tag already exists for given name, return that tag. Otherwise, create new tag and return it.  
  
You must subsequently call `$tag->save()` to store in db if `$tag->is_saved()` returns false, meaning it is a new tag.  
  
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
  
