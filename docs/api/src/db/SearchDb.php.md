<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/db/SearchDb.php  
  
# class DecaturVote\SearchDb  
  
See source code at [/src/db/SearchDb.php](/src/db/SearchDb.php)  
  
## Constants  
  
## Properties  
- `protected string $orm_namespace = 'DecaturVote\\SearchDb';`   
  
## Methods   
- `public function get_orm_class(string $table): string`   
- `public function create_tag(string $uuid, string $tag_name, string $tag_type, string $tag_description = ''): SearchDb\Tag` Create a tag, or update the tag if one already exists for $uuid  
  
Note: You CAN delete a search item AFTER a tag has been created, while preserving the tag.  
  
- `public function add_searchable(string $uuid, string $type, string $title, string $body, string $uri, \DateTime $published_at = new \DateTime()): SearchDb\Search` Add or UPDATE a searchable item.  
- `public function delete_from_search(string $uuid): bool` Delete an item from the search index by its uuid  
  
- `public function delete_from_tags(string $uuid): bool` Delete a tag associated with an item having the given uuid. Does NOT delete tagged_by entries  
  
- `public function delete_tagged_by(string $item_uuid): void` Delete all `tagged_by` entries for the given item.  
  
- `public function search(string $search_term, array $types = [], array $tag_ids  [], string $limit'LIMIT 0,30', int &$total_row_count  null): array` Get search results as an array of Search orms.  
  
- `public function feed(string $search_term, array $types = [], array $tag_ids  [], string $limit'LIMIT 0,30', int &$total_row_count  null): array` Get a feed of search results as an array of Search orms, ordered by datetime  
  
- `public function tags_from_search(string $search_term, array $types = [], array $tag_ids  []): array` Get search results as an array of Search orms.  
  
- `public function types_from_search(string $search_term, array $types = [], array $tag_ids  []): array` Get search results as an array of Search orms.  
  
- `public function build_search_query(string $search_term, array $types = [], array $tag_ids  [], string $limit  'LIMIT 0,30')`   
- `public function get_tags_in(array $int_ids): array` Get Tags where `id` is in the list of ids.  
  
- `public function get_tags_by_uuids(array $uuids): array` Get Tags where `tag_uuid` is in the list of uuids.  
  
- `public function get_item_tag(string $item_uuid): \DecaturVote\SearchDb\Tag` Get the tag that is uniquely identified by the given $item_uuid.  
  
- `public function get_items_tagged_by(string $tag_uuid): array` Get an array of search items that are tagged by the given Tag uuid.   
Use `$db->get_tags_on_item($item_uuid)` to get Tags instead of Search items.  
  
In some applications, a tag & tagged_by entry may persist even if the search item has been deleted.   
  
- `public function tag_item(string $item_uuid, string $tag_uuid)` Tag an existing search item.  
  
- `public function get_search_item(string $item_uuid): \DecaturVote\SearchDb\Search` Get an existing Search Item from a source item's uuid  
  
- `public function get_tags_on_item(string $item_uuid): array` Get an array of tags that the given item is tagged by  
  
  
