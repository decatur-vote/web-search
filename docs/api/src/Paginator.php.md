<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Paginator.php  
  
# class DecaturVote\Search\Paginator  
  
See source code at [/src/Paginator.php](/src/Paginator.php)  
  
## Constants  
  
## Properties  
- `public int $current_page;`   
- `protected int $page_size;`   
- `protected int $num_results;`   
- `protected string $base_url;`   
- `protected array $params;`   
- `protected int $num_pages;`   
  
## Methods   
- `public function __construct(int $current_page, int $page_size, int $num_results, string $base_url, array $params)`   
- `public function setNumResults(int $num_results)` Set the size of the resultset  
- `public function get_limit(): string` Get the sql limit  
  
- `public function get_links(int $num_links=11): array`   
- `public function get_range(int $cur_page, int $num_pages, int $range_size, int $start_page, int $end_page): void` Generate a pagination range, where we TRY to put the current page directly in the middle of the start page and end page, but we guarantee not to go outside the bounds of start_page>=0 and end_page < num_pages.  
  
  
