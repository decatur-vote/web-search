<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/src/SearchIntegration.php  
  
# class DecaturVote\Search\Test\SearchIntegration  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct(protected \PDO $pdo)`   
- `public function getPdo(): \PDO` get a valid PDO instance  
- `public function page_will_display(\Lia $lia): void` called when the page view is loaded.   
- `public function tag_feed_will_display(\Lia $lia): void`   
- `public function add_tag_will_display(\Lia $lia): void`   
- `public function can_create_tag(Lia $lia, Tag $tag): bool`   
- `public function getHostWebsite(): string`   
- `public function getRssChannel(): array`   
  
