# Status
Notes taken during & for development purposes

## Nov 6, 2023

I've recently added some convenience features for querying & new sql queries & i cleaned up the repo bigly, and I added a bigdb cli. git log is below

- 112472b - `(HEAD -> v1.0, origin/v1.0) composer update bigdb (fixes cli issue) (2 minutes ago) <Reed Sutman>`
- cd86ed8 - `lots of repo cleanup. add bigdb cli. stop using hidden directories. updated config files (2 minutes ago) <Reed Sutman>`
- 85d9d5d - `add Search::add_tag_by_item_uuid() & SearchDb::get_tags_by_item_uuids(array $uuids) (62 minutes ago) <Reed Sutman>`
- fe20107 - `run scrawl (2 hours ago) <Reed Sutman>`
- d00c1dc - `add some queries & convenience getter methdos (2 hours ago) <Reed Sutman>`
- 0b5abf4 - `fix query get_num_items_that_tag_item_uuid,. In get_db_row() on tag, limit the tag_name to 64 characters. (22 hours ago) <Reed Sutman>`
- b96f5ba - `improve some sql documentation. add tagged_by.get_num_items_that_tag_item_uuid query... what an awful query name lol, whatever. (23 hours ago) <Reed Sutman>`
- 5a382b8 - `composer update (23 hours ago) <Reed Sutman>`
- 2d76709 - `docs (23 hours ago) <Reed Sutman>`
- ab5e849 - `composer update (23 hours ago) <Reed Sutman>`

## Nov 3, 2023
move by_item_uuid from tagged_by to tag, since it returns tag rows. created a new by_item_uuid in tagged_by to return tagged_by instances. updated TaggedBy class to actually work with tagged_by db rows. updated tag-input.php to use tag.tagged_by query. recompiled queries.

Note: This repo is not well tested. TaggedBy doesn't have any tests. Changing code for tagged by didn't break any existing tests. the tag-input.php view apparently isn't tested either, bc there were no errors there when i moved the by_item_uuid query but hadn't yet updated it in the view.

## Oct 25, 2023
Added `class DecaturVote\Search\LiaisonPackage` for easier setup.

## Oct 4, 2023 Summary
Basically, what's still needed is:
- A BigOrm integration to allow any item to easily add itself & its tags to the search db.
- document all of what works (only some of it is currently documented)
- (POSTPONE) GUI for viewing, editing, and deleting tags (this would be great, but it is likely not a priority. Note that the search db is NEVER the original source of truth. This means any edited tags could be deleted in a search db refresh.)
- (POSTPONE) bulk-add tags to search-results (again, this is an issue because the search db is NOT a source of truth, so some kind of call back would be important here, so the source item can accept the updated tag).

## Oct 4, 2023
Added `can_create_tag` to test search integration.

What is the state of Search & what else needs to be done, if anything?

What works:
- GUI:
    - Search by query string, by tag, and by type all work very well
    - Lookup tag. Create tag from lookup input.
- Integration:
    - CSS styles
    - PHP interface for certain page/view displays, `can_create_tag`, get pdo, get rss channel, and get host website.
- Documentation:
    - Introduction + installation
    - Code examples (partial)
    - Routes (partial)
    - PHP API (partial)
    - CLI
    - How to test
- Routes:
    - `GET /search/` (optional query string & format & pagination)
    - `GET /feed/` (very similar to search)
    - `GET /search/create-tag/` to create a tag
    - `GET /search/get-tags/` to get list of tags as json
- CLI:
    - `bin/dv-search create-db` to create search database. (uses migrations)
- PHP API to:
    - `add_searchable(...)`
    - `$items = $db->search('query_string');`, and `feed()`
    - `$db->delete_from_search($uuid);`
    - `$db->create_tag()`, `delete_from_tags()`, and `delete_tagged_by()`
    - (internal) `$db->tags_from_search()`, `types_from_search()`, `build_search_query()`
    - `$lia->view('search/tag-input', ['item_uuid'=>$uuid])` - displays search input on a form.
    - `$search_orm->addTag(Tag $tag_orm)` & `add_tag_by_id(int id)`
    - `DecaturVote\SearchDb\Tag::tag_from_name(SearchDb $db, string $name)`
- Additional Features
    - Pagination
    - Migrations
    - Stored queries (mainly for internal use)


What's missing:
- Integration:
    - BigDb ORM Integration (i.e. an interface that lets any orm add themselves & their tags to the search db)
    - (no) Paginator customization (NOT A PRIORITY)
    - (no) Custom subclasses for search & tag orms (cando by overriding SearchDb class. Probably leave it that way)
- PHP API:
    - all good, i think
- Routes:
    - all good, i think
- Documentation:
    - A bunch of stuff.
- Other Features:
    - GUI for viewing, editing, and deleting tags
    - GUI for Bulk tagging. Ex: Perform a text-based search, then choose a tag to add, then click a checkbox for any articles that should gain that tag.

## Aug 31, 2023
tag-input is much improved, tags can now be created from tag-input, modified the integration interface with a can create tag method. Added some convenience sql & methods for working with tags & tagged\_by

## Aug 29, 2023
I significantly refactored the search page. I added SearchControls.js and SearchRows.js. I added a lot more functions, and just better organized the code. I added a lot of docblocks.

SearchRows.js is just value objects to represent what's returned from json_results.php
SearchControls.js is the actual autowire classes for managing interface.

I added a button template, and js to manage buttons. If js disabled, uses checkboxes. Otherwise fully uses buttons.

Other notes, copied from dv site:
Search is much better. got buttons in it. I still can't restore search db properly bc the search.json returns a lot of articles with an empty uuid & this is a unique column. I have `git pull`ed the backup repo, then deleted all the empty uuid rows from search.json (in the offline local copy). Those changes should NOT be committed. And of course nothing is tagged. It works though. I'll still want to add sorting options, but that might come at a later date.


## Aug 24, 2023
In DV Integrations, I started work on stylying the search tag "buttons". I changed the javascript to auto-add 'selected' class to the label of any tag that is selected. 

In integrations, I used opacity:0 to sort of hide the checkboxes, but they still take up space. The label class ".selected" addition fails if I click directly on the (invisible) checkbox, and not on the label.

This approach is also not accessibility friendly. I think I might change to pure buttons + javascript and abandon a javsacript-free dynamic searching model. Idk. If I keep it the way it is, I'm going to require a lot of hinky hacky solutions. I think the easiest thing will be to just make them buttons. Idk. We'll see. Maybe I use buttons when JS enabled but otherwise use checkbox+label. So maybe I'd print just the labels&inputs, then use javascript to replace them with buttons & hide the label but still use the checkboxes for the form ... omg idk. I need a good, accessible solution, and preferably one that's not too horribly convoluted.

## Aug 16, 2023
Feeds work. Search works. Pagination works. RSS & Json work.

I think all that's left is:
- DONE A nice way to add tags to an article or something. Like, js that looks up tags & adds them to a form. Then the server-side would need to actually handle it
- Review documentation for accuracy & thoroughness (maybe)
- Create an interface for items that wish to interact with search. 
    - Purpose is to be able to bulk-process search additions. Primarily for re-creating the search database. Could be useful for submission, too.

In the News app (separate repo), I did start some search integrations, but not by implementing an interface, and I'm not sure how far that effort went.


## June 7, 2023
added feeds, did a bunch of stuff. currently working on `/feed/{tag}.php`. A prototype is already working. It actually might just be good to go? idk.

I'm not going to use `/feed/{tag-name}/` at this time. It would be nice, but it isn't necessary, and it introduces other complications.

## June 6, 2023
added counts to tag list & type list. Only show tags available in current query. Update tag list & type counts when querying via js

started pagination (just getting total_row_count)

added a no results found heading

TODO:
- DONE pagination (*I HATE making pagination*)
- NO refactor, general cleanup - the relationship between index.php, page.php, and json_results.php is ... bad. I don't like it. I'm also not very happy about the various search-related functions (actual search, getting tag list, getting type list). It's just so messy. And now that I'm adding pagination, it is even worse. I MIGHT want to add a SearchQuery object which organizes all that stuff & makes it cleaner to interact with. SearchDb could still house convenience methods, but not have spread out confusing, disorganized logic.
    - It's mostly fine. Probably not gonna mess with it.
- Add feeds - json, html, and rss. Maybe I can just use the main search page for this with a single tag selected, but I think it would be nicer to have something simple like `/feed/tag-name/` or `/feed/article/` or `/feed/?tag=tag-name&article=article-name`. I think I could also use a feed-view, so I can display a feed on a particular page. It might be nice to have a `feed` table, then querying for a feed becomes extremely simple ... but it really just sounds like database duplication and I don't think I like it. Then again, feeds are probably purely datetime based, not algorithm-rank based. I guess a feed could include with a search term. 
    - DONE feed page
    - DONE Feed view, for displaying in random whatever spots.
- Document tagging (like adding tags to an article i'm writing)
- Review documentation to ensure its accuracy and thoroughness. 
- MAYBE provide some type of interface integration for adding items to the search table - like an ORM object might implement the interface, then have callbacks for adding to search table. This could enable bulk-querying & initialization of the search table.
    - Meaning: Something else wants itself to be added to search, so that object implements the interface, then BigDb will auto-call whatever implemented method? something like that.
- DONE add datetime column to search items that mirrors the source item's datetime.

## June 2, 2023

TODO:
- DONE When creating a tag, add that tag to the reference search item. If the 'city council office' creates a tag 'city-council', and someone searches for items with the tag 'city-council', the office absolutely should come up. There might be other edge cases where this is not true ... (but idc & i'm not controlling for that right now)
- NO Probably remove `type` from tags (NO. `tag` type is for programmatic reasons. On the search page, you probably don't want to allow filtering by literally ever tag that's available, since tags can also be used for feeds, and not just as search-filtering).
- DONE Add `Search For` to search page - allowing you to filter the `type` of item being searched for
- DONE Add tag-filtering to search page
- Just improve the heck out of the search stuff
    - DONE get updated tag list on query & only display tags that are found in the searched-for-items + display the count of search items tagged by each tag.
    - DONE Do same for type??
        - Correction: Always display all types, showing `0` as needed.
    - DONE Add a 'no results found'
    - DONE add item type to the search results
    - pagination
        - STARTED. I'm getting the total_row_count, but i haven't started implementing anything yet.

Made a lil video example & posted to youtube.

## June 1, 2023
Search needs more features, mainly tagging. 

Tagging:
- `tag` table to track all tags that can be added to an item
    - Should there be a tag TYPE? On a person's topic page, there will be a list of articles & stuff in which they're tagged. Additionally, articles can be @mentioned ... should articles also have a list of everything that mentioned them? A big part of me does not want to create tags for articles because it ... makes TOO MANY TAGS and they have a VERY specific purpose, not a broad purpose like a tag for an office or a person. So, I think adding a type to the tag table would help with that mess. Indexing & querying could also be made faster that way. And then we can avoid name collissions too, probably.
- `tagged_by` table to track what tags are on a given item (and to find items that have a given tag)
- improve searching for items
- create feeds of items
- Create tags for @mentions
    - the @mention in an article does not CREATE a tag - it references a tag
    - @Will Wetzel would reference the Will Wetzel TAG, which has the Will Wetzel Topic as its ... THING
    - So tags point to things. articles point to tags. Topics display all items that have tagged the topic
- Add search items that come from external sources - H&R articles could be search items, and then they could also be integrated with my tags. These would need a tag added to them to mark the source - to say that they are FROM herald & review. 

I scaffolded some early sql in the db folder. brain fried, can't think. Putting this down for the day.

## May 30, 2023
Init repo. Search already exists in the decatur vote monolith repo, and is being moved here. (move complete)

Search is functional on the dv site using this search app. The integration is still a mishmash crumheck. I need to go add an interface for integration & remove all funky code. Styling is needed (probably by the implementor, not by this library) & some css classes need to be changed.  

My search result seems to be showing new lines in the summary? Idk why. My integration currently is failing to set the uuid, i think (that's on the DV-side, not in this library, probably).

I gotta clean up / remove all the old code from diffsdb & diffs viewer. Just look over the repo & remove stuff that we're done with.

Need to write good documentation. Probably should add at least SOME tests.
