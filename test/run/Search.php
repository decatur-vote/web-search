<?php

namespace DeaturVote\Search\Test;

class Search extends \Tlf\Tester {

    public function getPdo($dbname='idontcare'){
        $pdo = require($this->file('test/pdo.php'));
        $searchDb = new \DecaturVote\SearchDb($pdo);
        $searchDb->recompile_sql();
        $searchDb->exec('search.create');
        return $pdo;
    }

    public function only_title(\Tlf\BigOrm $item){
        return $item->title;
    }

    public function testFeedWithPublishedAt(){
        $searchDb = new \DecaturVote\SearchDb($this->getPdo());

        $dates = [
            ['title'=>'a long title', 'date'=>'2024'],
            ['title'=>'title', 'date'=>'2023'],
            ['title'=>'a title', 'date'=>'2022'],
        ];
        foreach ($dates as $index=>$item){
            $dt = \DateTime::createFromFormat('Y', $item['date']);
            $title = $item['title'];
            $searchDb->add_searchable(uniqid('search'), 'article', $title, 'no summary', '/article/title/', $dt);
        }


        $items = $searchDb->feed('title');
        $titles = array_map(function($item){return $item->title;}, $items);
        $this->compare_arrays(
            ['a long title', 'title', 'a title'],
            $titles,
        );
        

        $items = $searchDb->search('title');
        $titles = array_map(function($item){return $item->title;}, $items);
        $this->compare_arrays(
            ['title', 'a long title', 'a title'],
            $titles,
        );

    }

    public function testGetAllTypes(){
        $searchDb = new \DecaturVote\SearchDb($this->getPdo());

        $searchDb->add_searchable(uniqid('search'), 'article', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'blog', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'blog', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'office', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'office', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'office', 'Title', 'Summary', '/article/title/');

        $types = $searchDb->query_rows('search.all_types');

        $this->compare_arrays(
            [
                ['type'=>'office','c'=>3],
                ['type'=>'blog','c'=>2],
                ['type'=>'article', 'c'=>1],
            ],
            $types
        );
        // print_r($types);
    }

    public function testWithTypes(){
        $pdo = $this->getPdo();
        $searchDb = new \DecaturVote\SearchDb($pdo);

        $searchDb->add_searchable(uniqid('search'), 'article', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'blog', 'Title', 'Summary', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'blog', 'Title', 'Summary', '/article/title/');

        $items = $searchDb->search('title');

        $this->test("No type gets all results.");
        $this->compare(3, count($items));


        $article_items = $searchDb->search('title',['article']);
        $blog_items = $searchDb->search('title',['blog']);

        $this->test("Type restricts query");
        $this->compare(1, count($article_items));
        $this->compare(2, count($blog_items));

        $union_items = $searchDb->search('title',['article','blog']);

        $this->test("Union returns all items.");
        $this->compare(3, count($union_items));

    }

    public function testQueries(){
        $pdo = $this->getPdo();
        $searchDb = new \DecaturVote\SearchDb($pdo);

        $searchDb->add_searchable(uniqid('search'), 'article', 'Title 1 x', 'Summary v', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'article', 'Title 2 v', 'Summary x', '/article/title/');
        $searchDb->add_searchable(uniqid('search'), 'article', 'Title 3 y', 'Summary r', '/article/title/');

        $items = $searchDb->search('2');

        $this->test("Query matching one item");
        $titles = array_map([$this, 'only_title'],$items);

        $this->compare_arrays(
            [ 'Title 2 v',],
            $titles
        );

        $this->test("Query matching two items");
        $items = $searchDb->search('x');
        $titles = array_map([$this, 'only_title'],$items);
        $this->compare_arrays(
            ['Title 1 x', 'Title 2 v'],
            $titles
        );

    }

}
