<?php

namespace DeaturVote\Search\Test;

class Tag extends \Tlf\Tester {

    public function getPdo($dbname='idontcare'){
        $pdo = require($this->file('test/pdo.php'));
        $searchDb = new \DecaturVote\SearchDb($pdo);
        $searchDb->recompile_sql();
        $searchDb->exec('search.create');
        $searchDb->exec('tag.create');
        $searchDb->exec('tagged_by.create');
        return $pdo;
    }

    public function testGetTagsForSearch(){
        // $pdo = $this->getPdo();
        // $pdo = require($this->file('test/pdo.php'));
        $pdo = $this->getPdo();
        require($this->file('test/src/init_database.php'));

        $searchDb = new \DecaturVote\SearchDb($pdo);


        $this->test('4 items for each tag');
        $tags = $searchDb->tags_from_search('board');

        $clean = [];
        foreach ($tags as $t){
            $clean[] = [
                'name'=>$t->tag_name,
                'count'=>$t->item_count
            ];
        }
        $this->compare_arrays(
            [
                ['name'=>'county-board', 'count'=>4],
                ['name'=>'school-board', 'count'=>4],

            ],
            $clean
        );


        $this->test("1 office for each tag");
        $tags = $searchDb->tags_from_search('board', ['office']);

        $clean = [];
        foreach ($tags as $t){
            $clean[] = [
                'name'=>$t->tag_name,
                'count'=>$t->item_count
            ];
        }
        $this->compare_arrays(
            [
                ['name'=>'county-board', 'count'=>1],
                ['name'=>'school-board', 'count'=>1],

            ],
            $clean
        );



        $this->test("3 articles for each tag");
        $tags = $searchDb->tags_from_search('board', ['article']);

        $clean = [];
        foreach ($tags as $t){
            $clean[] = [
                'name'=>$t->tag_name,
                'count'=>$t->item_count
            ];
        }
        $this->compare_arrays(
            [
                ['name'=>'county-board', 'count'=>3],
                ['name'=>'school-board', 'count'=>3],

            ],
            $clean
        );

    }

    public function testTags(){
        $searchDb = new \DecaturVote\SearchDb($this->getPdo());
        
        // I want to:
        // 1. create a search item
        // 2. create a tag FROM that search item (so essentially that search item is a tag)
        // 3. Create three new identical search items
        // 4. Add the tag to ONE of the search items
        // 5. Query via text (should get all three)
        // 6. Query via text WITH tag (should only get one)

        $uuid1 = uniqid();
        $uuid2 = uniqid();
        $uuid3 = uniqid();
        $uuid4 = uniqid();
        $searchDb->add_searchable($uuid1, 'office', 'Decatur City Council', 'Decatur City Council is 7 members, one of them being the mayor, and they govern the City of Decatur, Illinois.', '/city-council/'.$uuid1.'/');

        $tag_name = 'city-council';
        $tag_type = 'office';
        $tag_description = 'I might remove descriptions';
        $tag = $searchDb->create_tag($uuid1, $tag_name, $tag_type, $tag_description);
        

        $searchDb->add_searchable($uuid2, 'article', 'Article about city council', 'Its an article talking about city council', '/city-council/'.$uuid2.'/');
        $search3 = $searchDb->add_searchable($uuid3, 'article','Article about city council', 'Its an article talking about city council', '/city-council/'.$uuid3.'/');
        $searchDb->add_searchable($uuid4, 'article','Article about city council', 'Its an article talking about city council', '/city-council/'.$uuid4.'/');

        $search3->add_tag($tag);

        $all_items = $searchDb->search('city council');

        $this->test("All items returned");
        $this->compare(4, count($all_items));


        // search could take tag uuid, `office:city-council` (type:name), a full tag instance, or `office, city-council` (type, name), or even just tag_id
        $two_item_list = $searchDb->search('city council', [], [$tag->id]);
        $this->test("Two items returned");
        // it returns two items because the office is tagged by its own tag + one article is tagged
        $this->compare(2, count($two_item_list));

        // print_r($searchDb->to_arrays($two_item_list));
    }
}
