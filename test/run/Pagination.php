<?php

namespace DeaturVote\Search\Test;

class Pagination extends \Tlf\Tester {

    public function testPaginationRanges(){
        $tests = [
            [
                'page_number' => 0,
                'num_links' => 7,
                'num_pages' => 12,
                'expected_output' => [
                    'start_page' => 0,
                    'end_page' => 6
                ]
            ],
            [
                'page_number' => 4,
                'num_links' => 7,
                'num_pages' => 12,
                'expected_output' => [
                    'start_page' => 1,
                    'end_page' => 7
                ]
            ],
            [
                'page_number' => 9,
                'num_links' => 7,
                'num_pages' => 12,
                'expected_output' => [
                    'start_page' => 5,
                    'end_page' => 11
                ]
            ],
            [
                'page_number' => 10,
                'num_links' => 7,
                'num_pages' => 12,
                'expected_output' => [
                    'start_page' => 5,
                    'end_page' => 11
                ]
            ],
            [
                'page_number' => 3,
                'num_links' => 7,
                'num_pages' => 4,
                'expected_output' => [
                    'start_page' => 0,
                    'end_page' => 3
                ]
            ],
            [
                'page_number' => 0,
                'num_links' => 7,
                'num_pages' => 4,
                'expected_output' => [
                    'start_page' => 0,
                    'end_page' => 3
                ]
            ]
        ];

        foreach ($tests as $case){
            $paginator = new \DecaturVote\Search\Paginator($case['page_number'],1,1,'/',[]);           
            $paginator->get_range($case['page_number'], $case['num_pages'], $case['num_links'], $start_page, $end_page);
            $this->compare_arrays($case['expected_output'],
                [
                    'start_page'=>$start_page,
                    'end_page'=>$end_page,
                ]
            );
        }
    }

}
