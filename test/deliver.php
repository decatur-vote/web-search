<?php

require_once(__DIR__.'/../vendor/autoload.php');
require_once(__DIR__.'/src/SearchIntegration.php');

$pdo = require(__DIR__.'/pdo.php');

$searchDb = new \DecaturVote\SearchDb($pdo);
$searchDb->recompile_sql();

$lia = new \Lia();

// Add the built-in App, which provides all the web-server features.  
$server_app = new \Lia\Package\Server($lia, $fqn='lia:server');  // dir & base_url not required 
$lia->set('lia:server.resources.useCache', false);
$lia->debug = true;

$lia->router->varDelim = '\\.\\/\\:'; 

$lia->addResourceFile(\Tlf\Js\Autowire::filePath());
$lia->addRoute('/', function($route, $response) use ($pdo){
    $response->content = '<p>Visit <a href="/search/">Search page</a></p>'
        .'<p>Visit this page to re-create the database.'
        ;
    require(__DIR__.'/src/init_database.php');   

});

$lia->addRoute('/feed-test/',
    function($route, $response) use ($lia, $pdo, $searchDb){
        $response->content = $lia->view('search/tag-feed', ['tag'=>'city-council', 'type'=>'article']);
    }
);

$lia->addRoute('/add-tags/',
    function ($route, $response) use ($lia){
        $response->content = '<br><br><br>' 
            .'<h1>Add Tags</h1>'
            . $lia->view('search/tag-input') 
            . '<br><br><br>';
    }
);
  
// Add your app, providing home page & other core pages & features  
$integration = new \DecaturVote\Search\Test\SearchIntegration($pdo);
$site_app = new \DecaturVote\Search\LiaisonPackage($lia, $integration);


//
// delivers files in `site/public/*`   
$lia->deliver(); 
