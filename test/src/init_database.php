<?php
/**
 * @param $pdo must be passed to this script
 */

##########
## The claims made here about city council, county board, and school board are for testing only and may be inaccurate. 
##########

$searchDb = new \DecaturVote\SearchDb($pdo);
$searchDb->recompile_sql();
$searchDb->migrate(0,1);


// city council office + 3 articles
$searchDb->add_searchable($office1=uniqid(), 'office', 'City Council', 'no description', '/office/city-council/');
$council_tag = $searchDb->create_tag($office1, 'city-council', 'lets-remove-tag-type','Tag for Decatur City Council');

$searchDb->add_searchable($office_article1=uniqid(), 'article', 'City Council budget is 84 million', 'no description', '/article/city-budget/')
    ->add_tag($council_tag);
$searchDb->add_searchable($office_article1=uniqid(), 'article', 'City Council members vote down cannabis dispensaries', 'no description', '/article/city-cannabis/')
    ->add_tag($council_tag);
$searchDb->add_searchable($office_article1=uniqid(), 'article', 'City Council approves $250k rental assistance program', 'no description', '/article/city-rental-assistance/')
    ->add_tag($council_tag);


// county board office + 3 articles
$searchDb->add_searchable($office2=uniqid(), 'office', 'County Board', 'no description', '/office/count-board/');
$county_tag = $searchDb->create_tag($office2, 'county-board', 'lets-remove-tag-type','Tag for Macon County Board');

$searchDb->add_searchable($county_article1=uniqid(), 'article', 'gop County Board approved district maps; dems not happy', 'no description', '/article/district-maps/')
    ->add_tag($county_tag);
$searchDb->add_searchable($county_article1=uniqid(), 'article', 'County Board spends $27 million on new computer systems for courthouse', 'no description', '/article/27-mil-computers/')
    ->add_tag($county_tag);
$searchDb->add_searchable($county_article1=uniqid(), 'article', 'County Board now 12 republicans, 3 democrats', 'no description', '/article/county-board-party-makeup/')
    ->add_tag($county_tag);


// school board office + 3 articles
$searchDb->add_searchable($office3=uniqid(), 'office', 'School Board', 'no description', '/office/school-board/');
$school_tag = $searchDb->create_tag($office3, 'school-board', 'lets-remove-tag-type','Tag for Decatur Public Schools 61 school board');

$searchDb->add_searchable(uniqid(), 'article', 'School Board swears in 3 new members', 'no description', '/article/new-school-board/')
    ->add_tag($school_tag);
$searchDb->add_searchable(uniqid(), 'article', 'School Board expells students', 'no description', '/article/student-expulsions/')
    ->add_tag($school_tag);
$searchDb->add_searchable(uniqid(), 'article', 'School Board has narrow liberal majority', 'no description', '/article/liberal-majority-school-board/')
    ->add_tag($school_tag);
