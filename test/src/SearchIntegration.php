<?php  
  
namespace DecaturVote\Search\Test;

use Lia;
use DecaturVote\SearchDb\Tag;

class SearchIntegration implements \DecaturVote\Search\IntegrationInterface {  

    public function __construct(protected \PDO $pdo){}

    /** get a valid PDO instance */  
    public function getPdo(): \PDO {
        return $this->pdo;
    } 
  
    /** 
     * called when the page view is loaded. 
     * @param $lia the liaison instance  
     */  
    public function page_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/Search.css');
    }
    public function tag_feed_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/Search.css');
    }
    public function add_tag_will_display(\Lia $lia): void {
        $lia->addResourceFile(__DIR__.'/Search.css');
    }

    public function can_create_tag(Lia $lia, Tag $tag): bool {
        return true;
    }

    public function getHostWebsite(): string {
        // print_r($_SERVER);
        // exit;
        return 'http://localhost:'.$_SERVER['SERVER_PORT'];
    }

    public function getRssChannel(): array {
        return [
            'title'=>'Decatur Vote\'s FOSS Search',
            'link'=>'https://gitlab.com/decatur-vote/web-search',
            'description'=>'This is a search library, so you\'re probably seeing the test feed, or maybe the server is misconfigured.'
        ];
    }
}  
