# Web Search
This is the Search software for [DecaturVote.com](https://www.decaturvote.com). Built with [taeluf/liaison](https://www.taeluf.com/docs/Liaison) and [taeluf/big-db](https://www.taeluf.com/docs/Big%20Db). 

**Under Development** - This search library is functional, but it is still under development. We haven't documented anything tag-related. Some features may change or break.

See an [early example on youtube](https://www.youtube.com/watch?v=9nknPkhyZwI).


## Install
@template(php/composer_install)

## Routes  
At this time, route cannot be changed, because it is hardcoded in the javascript and PHP form.
- `GET /search/`: Load search page (html) with most recent search items
- `GET /search/?q=Some+Query`: Load search page (html) with results from the query `q`
- `GET /search/?format=json&q=Some+Query`: Get a json array of rows matching the query. The built-in search script uses this. Contains:
    - `uuid` - the item's uuid
    - `uri` - the url for the item. Typically a `/rel/url` for items on the same site, but can be a full url.
    - `title` - The title of the item
    - `summary` - A 300 character summary of the item. (*Search supports 1024 chars in the database, but only sends 300 characters to the browser. Well, 303 if you count the `...` at the end*).
    - `created_at` - mysql compatible datetime string
    - `updated_at` - user friendly format of `Y-m-d g:i a`

## Usage
Adding & deleting search items is very easy, and you can even use @see(docs/api/src/db/SearchDb.php.md, `SearchDb`) on its own to create your own GUI for search results.

Typically, you'll also want to do the Setup steps below.

Examples:
```php
<?php
// $pdo = new \PDO(...)
$searchDb = new \DecaturVote\SearchDb($pdo);

// add searchable items to the index.
$searchDb->add_searchable($uuid=uniqid('search'), 'Title 1 x', 'Summary v', '/article/title/1/');
$searchDb->add_searchable(uniqid('search'), 'Title 2 v', 'Summary x', '/article/title/2/');
$searchDb->add_searchable(uniqid('search'), 'Title 3 y', 'Summary r', '/article/title/3/');

// get an array of `DecaturVote\SearchDb\Search` items (they are BigOrm subclasses)
$items  = $searchDb->search("x"); # returns the first & 2nd item in that order, since they both contain x & title takes precedent. 

// delete an item by uuid
$searchDb->delete_from_search($uuid);
```

- See @see(docs/api/src/db/orm/Search.php.md, `DecaturVote\SearchDb\Search`) for the ORM properties
- See @see(src/db/sql/search.sql, `search.sql`) to see how results are ordered
- See @see(docs/api/src/db/SearchDb.php.md, `SearchDb`) for method definitions & param descriptions (including length limits!!!)

## Setup 
To setup the Search: Initialize the database, Set it up with Liaison, style it, and add an Integration class for needed callbacks. 

See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example.

### Initialize the database
Run this command in your CLI
```bash
vendor/bin/dv-search create-db -pdo path/to/pdo.php
```
You MUST create the file `path/to/pdo.php` and have it return a PDO instance. The table will be created with that instance.

### Setup with Liaison
```php
<?php
$integration = new \DecaturVote\Search\Test\SearchIntegration($pdo);
$site_app = new \DecaturVote\Search\LiaisonPackage($lia, $integration);

$lia->addResourceFile(\Tlf\Js\Autowire::filePath());
```

### Setup Integration
Define `MyIntegrationClass`. See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example. It must implement @see(src/IntegrationInterface.php, `DecaturVote\Search\IntegrationInterface`): 
```php
@file(src/IntegrationInterface.php)
```

### Styling
No styles are provided within. See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example. Here is a css template file for you to fill in.
```css
@file(src/view/search/page.css)
```

## Testing
### Setup Mysql test settings
At the root of `Search`, create the file `mysql_settings.json` and fill it in with your development environment settings:

**WARNING: Running tests will delete your search indices from your database**
```php
{
    "database": "dbname",
    "host": "localhost",
    "user": "user_name",
    "password": "bad_password"
}
```

Then run `vendor/bin/phptest`.
  
We currently don't have a Liaison integration in this repo, so testing in your browser is not available.

## Screenshots
These are from [DecaturVote.com/search/](https://decaturvote.com/search/). Styled by the [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations)

`/search/`:
![A 'Search' Heading, a search input box, and four search results in a vertical list, each showing a title, summary, and date + time. This screenshot shows design, and is not intended to share text-content. Some names are redacted with black boxes.](media/SearchPage.png "/search/")

