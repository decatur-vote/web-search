<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Web Search  
This is the Search software for [DecaturVote.com](https://www.decaturvote.com). Built with [taeluf/liaison](https://www.taeluf.com/docs/Liaison) and [taeluf/big-db](https://www.taeluf.com/docs/Big%20Db).   
  
**Under Development** - This search library is functional, but it is still under development. We haven't documented anything tag-related. Some features may change or break.  
  
See an [early example on youtube](https://www.youtube.com/watch?v=9nknPkhyZwI).  
  
  
## Install  
```bash  
composer require decatur-vote/web-search v1.0.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "decatur-vote/web-search": "v1.0.x-dev"}}  
```  
  
  
## Routes    
At this time, route cannot be changed, because it is hardcoded in the javascript and PHP form.  
- `GET /search/`: Load search page (html) with most recent search items  
- `GET /search/?q=Some+Query`: Load search page (html) with results from the query `q`  
- `GET /search/?format=json&q=Some+Query`: Get a json array of rows matching the query. The built-in search script uses this. Contains:  
    - `uuid` - the item's uuid  
    - `uri` - the url for the item. Typically a `/rel/url` for items on the same site, but can be a full url.  
    - `title` - The title of the item  
    - `summary` - A 300 character summary of the item. (*Search supports 1024 chars in the database, but only sends 300 characters to the browser. Well, 303 if you count the `...` at the end*).  
    - `created_at` - mysql compatible datetime string  
    - `updated_at` - user friendly format of `Y-m-d g:i a`  
  
## Usage  
Adding & deleting search items is very easy, and you can even use [`SearchDb`](/docs/api/src/db/SearchDb.php.md) on its own to create your own GUI for search results.  
  
Typically, you'll also want to do the Setup steps below.  
  
Examples:  
```php  
<?php  
// $pdo = new \PDO(...)  
$searchDb = new \DecaturVote\SearchDb($pdo);  
  
// add searchable items to the index.  
$searchDb->add_searchable($uuid=uniqid('search'), 'Title 1 x', 'Summary v', '/article/title/1/');  
$searchDb->add_searchable(uniqid('search'), 'Title 2 v', 'Summary x', '/article/title/2/');  
$searchDb->add_searchable(uniqid('search'), 'Title 3 y', 'Summary r', '/article/title/3/');  
  
// get an array of `DecaturVote\SearchDb\Search` items (they are BigOrm subclasses)  
$items  = $searchDb->search("x"); # returns the first & 2nd item in that order, since they both contain x & title takes precedent.   
  
// delete an item by uuid  
$searchDb->delete_from_search($uuid);  
```  
  
- See [`DecaturVote\SearchDb\Search`](/docs/api/src/db/orm/Search.php.md) for the ORM properties  
- See [`search.sql`](/src/db/sql/search.sql) to see how results are ordered  
- See [`SearchDb`](/docs/api/src/db/SearchDb.php.md) for method definitions & param descriptions (including length limits!!!)  
  
## Setup   
To setup the Search: Initialize the database, Set it up with Liaison, style it, and add an Integration class for needed callbacks.   
  
See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example.  
  
### Initialize the database  
Run this command in your CLI  
```bash  
vendor/bin/dv-search create-db -pdo path/to/pdo.php  
```  
You MUST create the file `path/to/pdo.php` and have it return a PDO instance. The table will be created with that instance.  
  
### Setup with Liaison  
```php  
<?php  
$integration = new \DecaturVote\Search\Test\SearchIntegration($pdo);  
$site_app = new \DecaturVote\Search\LiaisonPackage($lia, $integration);  
  
$lia->addResourceFile(\Tlf\Js\Autowire::filePath());  
```  
  
### Setup Integration  
Define `MyIntegrationClass`. See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example. It must implement [`DecaturVote\Search\IntegrationInterface`](/src/IntegrationInterface.php):   
```php  
<?php  
  
namespace DecaturVote\Search;  
  
interface IntegrationInterface {  
    /** get a valid PDO instance */  
    public function getPdo(): \PDO;  
  
    /** Get an array of RSS channel information, per the 2.0 RSS spec. See https://validator.w3.org/feed/docs/rss2.html */  
    public function getRssChannel(): array;  
  
    /** get a schem & host (like `https://example.com`) to prepend to any relative urls in your search items, primarily for rss */  
    public function getHostWebsite(): string;  
  
    /** called when the page view is loaded. You should use this to add your own styles.   
     *  
     * @param $lia the liaison instance  
     */  
    public function page_will_display(\Lia $lia): void;  
  
    /**  
     * Called when the view `search/tag-feed` is loaded. Use this to add styles.  
     */  
    public function tag_feed_will_display(\Lia $lia): void;  
  
    public function add_tag_will_display(\Lia $lia): void;  
  
    /**   
     * Check if tag creation is allowed for current user.  
     *  
     * @param $lia liaison instance  
     * @param $tag The tag that will be created, if allowed  
     *  
     * @return true if creation allowed, false if not.  
     */  
    public function can_create_tag(\Lia $lia, \DecaturVote\SearchDb\Tag $tag): bool;  
}  
```  
  
### Styling  
No styles are provided within. See [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations) for an example. Here is a css template file for you to fill in.  
```css  
/** The root node. Contains an h1, form and div.ResultsList */  
.DecaturVoteSearch {  
    --selected_button_color: green;  
}  
/** Search page heading */  
.DecaturVoteSearch h1 {}  
  
/** Parent Node for each .SearchResult */  
.DecaturVoteSearch .ResultsList {}  
  
/** Container for search result (child of .ResultsList) */  
.DecaturVoteSearch .SearchResult {}  
  
/** heading for each search result */  
.DecaturVoteSearch .SearchResult h2 {}  
/** search result title link */  
.DecaturVoteSearch .SearchResult h2 a {}  
/** search result title link, hover/active */  
.DecaturVoteSearch .SearchResult h2 a:hover, .DecaturVoteSearch .SearchResult h2 a:active, .DecaturVoteSearch .SearchResult h2 a:focus {}  
/** Summary of search result items */  
.DecaturVoteSearch .SearchResult > p {}  
  
.DecaturVoteSearch nav.pages a {  
    display:inline-block;  
    padding:4px;  
    margin:4px;  
}  
.DecaturVoteSearch nav.pages a.current_page {  
    text-decoration:none;  
}  
  
.DecaturVoteSearch a.reset_search {  
    font-style:italic;  
}  
  
.DecaturVoteSearch .SearchResult .search_footer {  
    display:flex;  
    flex-direction:row;  
    justify-content:space-between;  
}  
  
/** The datetime in the bottom-right corner of the result */  
.DecaturVoteSearch .SearchResult .date {  
    margin-right:8px;  
    margin-top:8px;  
}  
  
/** The item's type in the bottom-left corner of the result */  
.DecaturVoteSearch .SearchResult .type{  
    margin-right:8px;  
    margin-top:8px;  
}  
  
/** the search form */  
.DecaturVoteSearch form {}  
/** the search form's input */  
.DecaturVoteSearch form input {}  
  
.DecaturVoteSearch form button.SearchTagButton.selected {  
    background: var(--selected_button_color);  
}  
```  
  
## Testing  
### Setup Mysql test settings  
At the root of `Search`, create the file `mysql_settings.json` and fill it in with your development environment settings:  
  
**WARNING: Running tests will delete your search indices from your database**  
```php  
{  
    "database": "dbname",  
    "host": "localhost",  
    "user": "user_name",  
    "password": "bad_password"  
}  
```  
  
Then run `vendor/bin/phptest`.  
    
We currently don't have a Liaison integration in this repo, so testing in your browser is not available.  
  
## Screenshots  
These are from [DecaturVote.com/search/](https://decaturvote.com/search/). Styled by the [DecaturVote.com integrations](https://gitlab.com/decatur-vote/decaturvote.com-integrations)  
  
`/search/`:  
![A 'Search' Heading, a search input box, and four search results in a vertical list, each showing a title, summary, and date + time. This screenshot shows design, and is not intended to share text-content. Some names are redacted with black boxes.](.media/SearchPage.png "/search/")  
  
